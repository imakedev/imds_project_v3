create or replace PROCEDURE "MD_STP_MAPPING_SUM" -- OK

 (
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  
  CURSOR C1 IS SELECT
    m.PARENT_PART,
    min(m.MC_CODE) as MC_CODE
    -- count(m.MC_CODE) as mccount
  FROM MD_TBL_PART_REPORT9 m
    -- where m.PARENT_PART='1005C643'
  GROUP BY
    m.PARENT_PART
  ;
  CURSOR C2 (C2_PARENT_PART IN varchar2,C2_MC_CODE IN varchar2)
  IS
     SELECT model.*,ROWID -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9 model -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
       where 
       -- SUPPLIER_ID='Y067' and 
      -- PARENT_PART='1584A906'
      model.PARENT_PART=C2_PARENT_PART
      and model.MC_CODE=C2_MC_CODE
      ORDER BY model.PARENT_PART,model.MC_CODE,to_number(model.LEVEL_SEQ)
      ;
    
  W_C1 C1%ROWTYPE;
  W_C2 C2%ROWTYPE; 
BEGIN 
commit;
   delete from MD_TBL_PART_SUM r1; 
   commit;
  -- W_COUNT_ALL :=0;
 -- select SYSDATE into NOW_TIME from dual;
   OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
        BEGIN
        
              OPEN C2(W_C1.PARENT_PART,W_C1.MC_CODE);
                FETCH C2 INTO W_C2;
                   WHILE NOT(C2%NOTFOUND)
                   
                   LOOP
                    
                            insert into MD_TBL_PART_SUM (
                       SELECT
	GL_NO ,
	MC_CODE ,
	UPG_CODE ,
	SERIES_NO ,
	PLVC ,
	P_NAME,--PART_NAME ,
	PART_COMPOSITION ,
	PARENT_PART ,
	PART_NO ,
	DRAW_NO ,
	LEVEL_NO ,
	UPC_CODE ,
	SAFE_ID ,
	EO_NO ,
	CL ,
	DRAWING_MASS ,
	MASS_ID ,
	MATERIAL_CODE ,
	DRAWING_ID ,
	DRAWING_SPEC ,
	COLOR_ID ,
	CLS ,
	QUALITY_ID ,
	F_CLASSIFICATION ,
	QTY,-- to_number(QUANTITY) ,
	QUANTITY_ID ,
	PART_ID ,
	INSTALL_NO ,
	INSTALL_SYMBOL ,
	SEQ_NO ,
	ENTRY_GL_NO ,
	ENTRY_MC_CODE ,
	KD_SITUATION_ID ,
	COLID_ERR_ID ,
	DAIYOUZU_ID ,
	AR_ID ,
	CREATE_DATE , -- CREATE_DATE DATE,
	CREATE_BY , -- CREATE_BY VARCHAR2(30 byte),
	CREATE_IP , -- CREATE_IP VARCHAR2(30 byte),
	UPDATE_DATE , -- UPDATE_DATE DATE,
	UPDATE_BY , -- UPDATE_BY VARCHAR2(30 byte),
	UPDATE_IP , -- UPDATE_IP VARCHAR2(30 byte)'||
  -- NOW_SEQ||' -- 
  SUPPLIER_CODE, --SUPPLIER_CODE VARCHAR2(6 byte),
											MAPPING_TYPE, -- MAPPING_TYPE VARCHAR2(20 byte),
											EPL_PART_NO, --  EPL_PART_NO VARCHAR2(255 byte),
											LOCAL_P_NO, -- LOCAL_P_NO VARCHAR2(255 byte),
											ORDER_P_NO,--ORDER_P_NO, -- ORDER_P_NO VARCHAR2(255 byte),
											MMC_APPR, -- MMC_APPR VARCHAR2(255 byte) ,
											ENG_DATA_P_NO,--PART_NAME, -- ENG_DATA_P_NO VARCHAR2(255 byte),
											INPUT_IMDS_P_NO, -- INPUT_IMDS_P_NO VARCHAR2(255 byte) ,
											LEVEL_SEQ,--to_char(to_number(SEQ_NO)), -- LEVEL_SEQ VARCHAR2(255 byte),
  -- null
   MD_SEQ_REPORT_MAPPING.nextval 
                    from MD_TBL_PART_REPORT9 model 
                    WHERE model.ROWID=W_C2.ROWID 
                    -- and model.PART_NO=W_PART_ORDER_NO and ROWNUM = 1
                  ); 
                              
                    -- END IF;
                  FETCH C2 INTO W_C2;
                END LOOP;
            CLOSE C2;
       END; 
            
      FETCH C1 INTO W_C1;
   END LOOP;
   CLOSE C1; 
   commit;
END;