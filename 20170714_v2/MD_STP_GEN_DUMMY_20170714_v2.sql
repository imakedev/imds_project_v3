create or replace PROCEDURE "MD_STP_GEN_DUMMY" -- OK
(
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  var1 VARCHAR2(500);
  v_process_status_d MD_TBL_CONTROL_MAPPING.PROCESS_STATUS_ID%TYPE;
  CURSOR C1 IS SELECT PARENT_PART -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9  -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
        -- where 
       -- SUPPLIER_ID='Y067' and 
         -- PARENT_PART='1005C643' -- and SUPPLIER_CODE != '-'
     -- PARENT_PART='3800A139' -- '1360A097'--'1584A906'
      -- where SUPPLIER_ID='S104' and PART_NO='1350A807V'
   --where SUPPLIER_ID='A035' and PART_NO='MN124448'
  -- where SUPPLIER_ID='S005' -- T664
   GROUP BY PARENT_PART
  ;
  
  CURSOR C2 (C2_PARENT_PART IN varchar2)
  IS
     SELECT model.*,ROWID -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9 model -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
       where 
       -- SUPPLIER_ID='Y067' and 
      -- PARENT_PART='1584A906'
      model.PARENT_PART=C2_PARENT_PART;
    
  W_C1 C1%ROWTYPE;
  W_C2 C2%ROWTYPE;
  W_COUNT NUMBER;
  W_COUNT_DUMMY NUMBER;
  W_LEVEL_NO_SUPP VARCHAR2(255);
  W_COUNT_DUMMY_STR VARCHAR2(255);
  W_PART_NO VARCHAR2(255);
  W_FORMAT VARCHAR2(255);
  W_DUMMY_PART_NO VARCHAR2(255);
BEGIN
-- update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=1 where PROCESS_ID='MAPPING';
  -- clear table for test
-- delete from MD_TBL_PART_REPORT9 r1;
W_FORMAT :='FM00';
commit;

  -- W_COUNT_ALL :=0;
 -- select SYSDATE into NOW_TIME from dual;
   OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
        BEGIN
           W_COUNT  := 0;
           W_COUNT_DUMMY :=0;
          -- BEGIN
              OPEN C2(W_C1.PARENT_PART);
                FETCH C2 INTO W_C2;
                   WHILE NOT(C2%NOTFOUND)
                   
                   LOOP
                   W_DUMMY_PART_NO :=null;
                      -- if  W_C2.SUPPLIER_CODE is not null THEN
                      IF (W_C2.LEVEL_NO=1) THEN
                        W_LEVEL_NO_SUPP :=W_C2.SUPPLIER_CODE ;
                      END IF;
                      
                      SELECT count(1) INTO W_COUNT
                        FROM MD_TBL_PART_MASTER2 mmth_part
                          where (mmth_part.PART_NO=W_C2.PART_NO --W_C2.ORDER_P_NO --W_C2.PART_NO 
                               -- and W_C2.PART_NO  != W_C2.PARENT_PART
                          and mmth_part.SUPPLIER_ID != W_C2.SUPPLIER_CODE 
                          and exists(
                                        SELECT * from MD_TBL_PART_MODEL2 model where model.PART_NO=mmth_part.PART_NO
                                     )
                          and  exists (
                         SELECT UPG_CODE
                            FROM MD_TBL_PART_MASTER2 m2
                              where m2.SUPPLIER_ID=W_C2.SUPPLIER_CODE and m2.PART_NO=W_C2.ORDER_P_NO 
                              and m2.UPG_CODE=mmth_part.UPG_CODE
                         )
                          )
                          or
                           (  SUBSTR(mmth_part.PART_NO,0,length(W_C2.PART_NO)) = W_C2.PART_NO 
                              --  SUBSTR(mmth_part.PART_NO,0,length(W_C2.ORDER_P_NO)) = W_C2.ORDER_P_NO 
                                and mmth_part.SUPPLIER_ID != W_C2.SUPPLIER_CODE
                                and  LENGTH(mmth_part.PART_NO) > 8
                                and exists(
                                        SELECT * from MD_TBL_PART_MODEL2 model where model.PART_NO=W_C2.PART_NO 
                                     )
                          and  exists (
                         SELECT UPG_CODE
                            FROM MD_TBL_PART_MASTER2 m2
                              where m2.SUPPLIER_ID=W_C2.SUPPLIER_CODE and m2.PART_NO=W_C2.ORDER_P_NO 
                              and m2.UPG_CODE=mmth_part.UPG_CODE
                         )
                      )
                         
                        ; 
                     /*  */
                       
                      IF W_COUNT > 0  THEN
                           -- W_MAP_TYPE:='2'; --W_MAP_TYPE:='2';
                           W_COUNT_DUMMY := W_COUNT_DUMMY+1;
                           -- W_IS_COUNT_DUMMY:='1';
                           
                           if W_COUNT_DUMMY > 99 THEN
                            W_FORMAT :='FM000';
                           END if;
                        SELECT  to_char( W_COUNT_DUMMY, W_FORMAT ) into W_COUNT_DUMMY_STR from dual;
                        W_PART_NO := 'DUMMY'||W_COUNT_DUMMY_STR;
                        
                         FOR dummy_item IN (
                           select SUPPLIER_ID ,mmth_part.PART_NO  FROM MD_TBL_PART_MASTER2 mmth_part
                          where (mmth_part.PART_NO=W_C2.PART_NO --W_C2.ORDER_P_NO --W_C2.PART_NO 
                               -- and W_C2.PART_NO  != W_C2.PARENT_PART
                          and mmth_part.SUPPLIER_ID != W_C2.SUPPLIER_CODE 
                           and exists(
                                        SELECT * from MD_TBL_PART_MODEL2 model where model.PART_NO=mmth_part.PART_NO
                                     )
                          and  exists (
                         SELECT UPG_CODE
                            FROM MD_TBL_PART_MASTER2 m2
                              where m2.SUPPLIER_ID=W_C2.SUPPLIER_CODE and m2.PART_NO=W_C2.ORDER_P_NO 
                              and m2.UPG_CODE=mmth_part.UPG_CODE
                         )
                          )
                          or
                           (  SUBSTR(mmth_part.PART_NO,0,length(W_C2.PART_NO)) = W_C2.PART_NO 
                              --  SUBSTR(mmth_part.PART_NO,0,length(W_C2.ORDER_P_NO)) = W_C2.ORDER_P_NO 
                                and mmth_part.SUPPLIER_ID != W_C2.SUPPLIER_CODE
                                and  LENGTH(mmth_part.PART_NO) > 8
                                and exists(
                                        SELECT * from MD_TBL_PART_MODEL2 model where model.PART_NO=W_C2.PART_NO 
                                     )
                          and  exists (
                         SELECT UPG_CODE
                            FROM MD_TBL_PART_MASTER2 m2
                              where m2.SUPPLIER_ID=W_C2.SUPPLIER_CODE and m2.PART_NO=W_C2.ORDER_P_NO 
                              and m2.UPG_CODE=mmth_part.UPG_CODE
                         )
                           )
                           GROUP BY SUPPLIER_ID,mmth_part.PART_NO
                           
                            )
                    LOOP
                    W_DUMMY_PART_NO := dummy_item.SUPPLIER_ID;
                    --dbms_output.put_line('varc = '||W_DUMMY_PART_NO||'mmth_part.PART_NO'||dummy_item.PART_NO);
                    
  
                    /* */
                    IF W_LEVEL_NO_SUPP !='-' THEN 
                            insert into MD_TBL_PART_REPORT9 (
                       SELECT
	GL_NO ,
	MC_CODE ,
	UPG_CODE ,
	SERIES_NO ,
	PLVC ,
	P_NAME,--PART_NAME ,
	PART_COMPOSITION ,
	PARENT_PART ,
	PART_NO ,
	DRAW_NO ,
	LEVEL_NO ,
	UPC_CODE ,
	SAFE_ID ,
	EO_NO ,
	CL ,
	DRAWING_MASS ,
	MASS_ID ,
	MATERIAL_CODE ,
	DRAWING_ID ,
	DRAWING_SPEC ,
	COLOR_ID ,
	CLS ,
	QUALITY_ID ,
	F_CLASSIFICATION ,
	QTY,-- to_number(QUANTITY) ,
	QUANTITY_ID ,
	PART_ID ,
	INSTALL_NO ,
	INSTALL_SYMBOL ,
	SEQ_NO ,
	ENTRY_GL_NO ,
	ENTRY_MC_CODE ,
	KD_SITUATION_ID ,
	COLID_ERR_ID ,
	DAIYOUZU_ID ,
	AR_ID ,
	CREATE_DATE , -- CREATE_DATE DATE,
	CREATE_BY , -- CREATE_BY VARCHAR2(30 byte),
	CREATE_IP , -- CREATE_IP VARCHAR2(30 byte),
	UPDATE_DATE , -- UPDATE_DATE DATE,
	UPDATE_BY , -- UPDATE_BY VARCHAR2(30 byte),
	UPDATE_IP , -- UPDATE_IP VARCHAR2(30 byte)'||
  -- NOW_SEQ||' -- 
  dummy_item.SUPPLIER_ID, --SUPPLIER_CODE VARCHAR2(6 byte),
											'9',--MAPPING_TYPE, -- MAPPING_TYPE VARCHAR2(20 byte),
											EPL_PART_NO, --  EPL_PART_NO VARCHAR2(255 byte),
											LOCAL_P_NO, -- LOCAL_P_NO VARCHAR2(255 byte),
											dummy_item.PART_NO,--ORDER_P_NO, -- ORDER_P_NO VARCHAR2(255 byte),
											MMC_APPR, -- MMC_APPR VARCHAR2(255 byte) ,
											ENG_DATA_P_NO,--PART_NAME, -- ENG_DATA_P_NO VARCHAR2(255 byte),
											INPUT_IMDS_P_NO, -- INPUT_IMDS_P_NO VARCHAR2(255 byte) ,
											LEVEL_SEQ,--to_char(to_number(SEQ_NO)), -- LEVEL_SEQ VARCHAR2(255 byte),
  -- null
   MD_SEQ_REPORT_MAPPING.nextval 
                    from MD_TBL_PART_REPORT9 model 
                    WHERE model.ROWID=W_C2.ROWID 
                    -- and model.PART_NO=W_PART_ORDER_NO and ROWNUM = 1
                  );
                  -- dbms_output.put_line('var1 = '||var1);
                  -- EXECUTE IMMEDIATE var1; 
                    UPDATE MD_TBL_PART_REPORT9
                        SET MAPPING_TYPE='2',PART_NO=W_PART_NO
                        WHERE ROWID=W_C2.ROWID ;
                        commit; 
                  commit;
                  END IF;
                 /*  */
                    END LOOP;
                      /*
                      if W_DUMMY_PART_NO != null THEN 
                      dbms_output.put_line('varc = '||W_DUMMY_PART_NO);
                      insert into MD_TBL_PART_REPORT9 (SUPPLIER_CODE,MAPPING_TYPE,EPL_PART_NO,LOCAL_P_NO,ORDER_P_NO
                    ,MMC_APPR,ENG_DATA_P_NO,INPUT_IMDS_P_NO,LEVEL_SEQ,GL_NO
                    ,MC_CODE ,UPG_CODE ,SERIES_NO ,PLVC ,P_NAME,PART_COMPOSITION ,PARENT_PART ,PART_NO ,DRAW_NO ,LEVEL_NO 
                    ,UPC_CODE ,SAFE_ID ,EO_NO ,CL ,DRAWING_MASS ,MASS_ID ,MATERIAL_CODE ,DRAWING_ID ,DRAWING_SPEC 
                    ,COLOR_ID ,CLS ,QUALITY_ID ,F_CLASSIFICATION ,QTY,QUANTITY_ID ,PART_ID ,INSTALL_NO ,INSTALL_SYMBOL ,SEQ_NO 
                    ,ENTRY_GL_NO ,ENTRY_MC_CODE ,KD_SITUATION_ID ,COLID_ERR_ID ,DAIYOUZU_ID ,AR_ID ,CREATE_DATE  
                    ,CREATE_BY ,CREATE_IP ,UPDATE_DATE ,UPDATE_BY , UPDATE_IP ,ORDER_ID)
                    values(W_DUMMY_PART_NO,W_C2.MAPPING_TYPE,W_C2.EPL_PART_NO,W_C2.LOCAL_P_NO,W_C2.ORDER_P_NO
                    ,W_C2.MMC_APPR,W_C2.ENG_DATA_P_NO,W_C2.INPUT_IMDS_P_NO,W_C2.LEVEL_SEQ,W_C2.GL_NO
                    ,W_C2.MC_CODE ,W_C2.UPG_CODE ,W_C2.SERIES_NO ,W_C2.PLVC ,W_C2.P_NAME,W_C2.PART_COMPOSITION ,W_C2.PARENT_PART 
                    ,W_C2.PART_NO ,W_C2.DRAW_NO ,W_C2.LEVEL_NO 
                    ,W_C2.UPC_CODE ,W_C2.SAFE_ID ,W_C2.EO_NO ,W_C2.CL ,W_C2.DRAWING_MASS ,W_C2.MASS_ID ,W_C2.MATERIAL_CODE 
                    ,W_C2.DRAWING_ID ,W_C2.DRAWING_SPEC 
                    ,W_C2.COLOR_ID ,W_C2.CLS ,W_C2.QUALITY_ID ,W_C2.F_CLASSIFICATION ,W_C2.QTY,W_C2.QUANTITY_ID ,W_C2.PART_ID 
                    ,W_C2.INSTALL_NO ,W_C2.INSTALL_SYMBOL ,W_C2.SEQ_NO 
                    ,W_C2.ENTRY_GL_NO ,W_C2.ENTRY_MC_CODE ,W_C2.KD_SITUATION_ID ,W_C2.COLID_ERR_ID ,W_C2.DAIYOUZU_ID 
                    ,W_C2.AR_ID ,W_C2.CREATE_DATE 
                    ,W_C2.CREATE_BY ,W_C2.CREATE_IP ,W_C2.UPDATE_DATE ,W_C2.UPDATE_BY , W_C2.UPDATE_IP 
                    ,MD_SEQ_REPORT_MAPPING.nextval );
              commit;
              END IF;
              */
                        -- dbms_output.put_line('ROWID = '||W_C2.ROWID||' W_PART_NO=' || W_PART_NO);
                        /*
                        UPDATE MD_TBL_PART_REPORT9
                        SET MAPPING_TYPE='2',PART_NO=W_PART_NO
                        WHERE ROWID=W_C2.ROWID ;
                        commit; 
                        */
                        /*
                         FOR dummy_item IN (
                           select *  FROM MD_TBL_PART_REPORT9 report9
                                  WHERE report9.ROWID=W_C2.ROWID 
                                  and ROWNUM =1
                            )
                    LOOP
                    BEGIN
                    */
                      
                  /*
                    END;
                    END LOOP;
                    */ 
                             
                      END IF;
                    -- END IF;
                  FETCH C2 INTO W_C2;
                END LOOP;
            CLOSE C2;
       END; 
            
      FETCH C1 INTO W_C1;
   END LOOP;
   CLOSE C1;
     
   /* */
     select CURRENT_TIMESTAMP into v_timestamp from dual;
     select PROCESS_STATUS_ID into v_process_status_d from MD_TBL_CONTROL_MAPPING where PROCESS_ID='MAPPING';
      
       --  update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=2 where PROCESS_ID='MAPPING';
     
   var1:='drop sequence MD_SEQ_REPORT_MAPPING';
 EXECUTE IMMEDIATE var1; 
 commit;
 -- select TO_CHAR(SYSDATE,'YYYYMMDD_HH24MISS') into NOW_SEQ from dual;
 -- var1:='CREATE SEQUENCE MD_SEQ_'||NOW_SEQ||'  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE';
 var1:='CREATE SEQUENCE MD_SEQ_REPORT_MAPPING  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE';
    EXECUTE IMMEDIATE var1; 
   
     -- MD_STP_INPUT_STATUS(P_CRATED_BY,P_CRATED_IP);
     commit;
     
      MD_STP_GEN_BY_SYS(P_CRATED_BY,P_CRATED_IP);
END;