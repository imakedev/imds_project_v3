create or replace PROCEDURE "MD_STP_INPUT_STATUS" -- OK

 (
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  v_process_status_d MD_TBL_CONTROL_MAPPING.PROCESS_STATUS_ID%TYPE;

  CURSOR C1 IS SELECT SUPPLIER_CODE,count(SUPPLIER_CODE) as TOTAL_RECORD 
  FROM MD_TBL_PART_REPORT9 -- MD_TBL_PART_REPORT2
  where SUPPLIER_CODE is not null
  GROUP BY SUPPLIER_CODE;
  W_C1 C1%ROWTYPE;
  W_SUPPLIER_CODE  MD_TBL_PART_REPORT9.SUPPLIER_CODE%TYPE;
  W_TOTAL_RECORD  NUMBER;
  
  W_SUM_OF_INPUT_RECORD NUMBER;
	W_PERCENT_REMAIN NUMBER(38,2);
	W_PERCENT_OF_INPUT_RECORD NUMBER(38,2);
  
  
  NOW_TIME DATE;
BEGIN
-- update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=1 where PROCESS_ID='MAPPING';
  -- clear table for test
delete from MD_TBL_PROC_INPUT_STATUS_V2 r1;
W_SUPPLIER_CODE:='';
commit;
  -- W_COUNT_ALL :=0;
  select SYSDATE into NOW_TIME from dual;
   OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
      W_SUPPLIER_CODE := W_C1.SUPPLIER_CODE ;
      W_TOTAL_RECORD  := W_C1.TOTAL_RECORD;
      
     --  W_MAP_TYPE         := NULL;
            -- type 1 KD_SITUATION_ID ( KD ID Replace ) ="*"
            BEGIN
            -- dbms_output.put_line(' W_SUPPLIER_CODE = '||W_SUPPLIER_CODE);
            SELECT count(PART_NO) into W_SUM_OF_INPUT_RECORD 
              from MD_TBL_PART_REPORT9
              WHERE SUPPLIER_CODE=W_SUPPLIER_CODE
              and PART_NO not like 'DUMM%' and MMC_APPR ='*'
              GROUP BY SUPPLIER_CODE;
              /*
              SELECT count(SUPPLIER_ID) into W_SUM_OF_INPUT_RECORD from MD_TBL_PART_STATUS2
                where SUPPLIER_ID=W_SUPPLIER_CODE
              GROUP BY SUPPLIER_ID;
              */
              EXCEPTION WHEN NO_DATA_FOUND THEN
               -- dbms_output.put_line(' *********  NO_DATA_FOUND = '||W_SUPPLIER_CODE);
                W_SUM_OF_INPUT_RECORD :=0;
           END;
                    insert into MD_TBL_PROC_INPUT_STATUS_V2 
                      (SUPPLIER_CODE,TOTAL_RECORD,SUM_OF_INPUT_RECORD,PERCENT_REMAIN,PERCENT_OF_INPUT_RECORD
                      ,INCLD_DUMMY,PROCESS_DATE,CREATE_DATE,CREATE_BY,CREATE_IP,UPDATE_DATE,UPDATE_BY
                      ,UPDATE_IP)
                    values 
                    (W_SUPPLIER_CODE,W_TOTAL_RECORD,W_SUM_OF_INPUT_RECORD,
                    100-to_number(((100*W_SUM_OF_INPUT_RECORD)/W_TOTAL_RECORD)),
                    to_number(((100*W_SUM_OF_INPUT_RECORD)/W_TOTAL_RECORD)),
                    
                    null,NOW_TIME,NOW_TIME,P_CRATED_BY,P_CRATED_IP,NOW_TIME,P_CRATED_BY,P_CRATED_IP);
               
                  commit;
                 --  END;
             -- dbms_output.put_line(' TYPE 1 Count = '||W_COUNT_ALL);
             -- END IF;
      FETCH C1 INTO W_C1;
      END LOOP;
      CLOSE C1;
      
       update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=2 where PROCESS_ID='MAPPING';
     commit;
  
END;