--------------------------------------------------------
--  File created - Wednesday-July-19-2017   
--------------------------------------------------------
REM INSERTING into MD_TBL_QUESTION_STATUS
SET DEFINE OFF;
Insert into MD_TBL_QUESTION_STATUS (QUESTION_STATUS_ID,QUESTION_STATUS_NAME) values ('1','Wait for Prepare');
Insert into MD_TBL_QUESTION_STATUS (QUESTION_STATUS_ID,QUESTION_STATUS_NAME) values ('2','Wait for Checker');
Insert into MD_TBL_QUESTION_STATUS (QUESTION_STATUS_ID,QUESTION_STATUS_NAME) values ('3','Wait for Approver');
Insert into MD_TBL_QUESTION_STATUS (QUESTION_STATUS_ID,QUESTION_STATUS_NAME) values ('4','Approved');
Insert into MD_TBL_QUESTION_STATUS (QUESTION_STATUS_ID,QUESTION_STATUS_NAME) values ('5','Wait for MMC Prepare');
