create or replace PROCEDURE "MD_STP_IMP_SUPPLIER" -- OK

 (
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  CURSOR C1 IS SELECT supp_text.*,ROWID -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_TEXT_SUPPLIER2 supp_text 
  ;
  W_C1 C1%ROWTYPE;
  W_COUNT NUMBER;
BEGIN
    delete from MD_TBL_SUPPLIER_MASTER2;
    commit;
    select CURRENT_TIMESTAMP into v_timestamp from dual;
   
    commit;
    OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
       BEGIN 
              SELECT count(1) INTO W_COUNT
              FROM MD_TBL_SUPPLIER_MASTER supp_master
              where supp_master.SUPPLIER_ID=W_C1.SUPPLIER_ID  
              ; 
              IF W_COUNT > 0 THEN
                  UPDATE MD_TBL_SUPPLIER_MASTER SET SUPPLIER_NAME_ENG=W_C1.SUPPLIER_NAME_ENG
                  ,UPDATE_DATE=v_timestamp,UPDATE_BY=P_CRATED_BY,UPDATE_IP=P_CRATED_IP
                  where SUPPLIER_ID=W_C1.SUPPLIER_ID;
                  commit;
              ELSE 
                  INSERT INTO MD_TBL_SUPPLIER_MASTER(
                          SUPPLIER_ID,SUPPLIER_NAME_ENG,
                          CREATE_DATE,CREATE_BY,CREATE_IP) 
                          values(W_C1.SUPPLIER_ID,W_C1.SUPPLIER_NAME_ENG,
                          v_timestamp,P_CRATED_BY,P_CRATED_IP);
                          commit;
              END IF;
       END;
       FETCH C1 INTO W_C1;
   END LOOP;
   CLOSE C1;
     /* */
     INSERT INTO MD_TBL_SUPPLIER_MASTER2(
     SUPPLIER_ID,SUPPLIER_NAME_ENG,
     CREATE_DATE,CREATE_BY,CREATE_IP,UPDATE_DATE,UPDATE_BY,UPDATE_IP) --- Columns of smaller table
        SELECT  SUPPLIER_ID,SUPPLIER_NAME_ENG,
       v_timestamp,P_CRATED_BY,P_CRATED_IP,v_timestamp,P_CRATED_BY,P_CRATED_IP  ---Columns of Master table
        FROM MD_TBL_TEXT_SUPPLIER2;
  -- UPG_CODE|?|PART_NO|PART_NAME|SUPPLIER_CODE|SUPPLIER_NAME
        commit;
       /* */
     update MD_TBL_CONTROL_IMPORT set PROCESS_STATUS_ID=5 where PROCESS_ID='IMPORT_SUPPLIER';
      update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=4 where PROCESS_ID='MAPPING';
     commit;
END;