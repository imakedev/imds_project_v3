package com.mmth.imds.backing.app;

import com.mmth.framework.util.ELog;
import com.mmth.framework.util.RzJDBCUtils;
import com.mmth.imds.model.InterFaceFileModel;
import com.mmth.imds.model.process.ImportModel;
import com.mmth.imds.service.IMDSServices;
import com.mmth.imds.threads.MappingMultipleRunner;
import com.mmth.mdf.controller.config.RzInitialConfig;
import com.mmth.mdf.controller.template.MDFTAPP01;

import java.io.File;

import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import javax.servlet.ServletContext;

import oracle.adf.view.rich.component.rich.layout.RichPanelGridLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.component.rich.output.RichMessages;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.event.PollEvent;
import org.apache.myfaces.trinidad.event.RangeChangeEvent;

public class MD0004P01 extends MDFTAPP01 {
    DecimalFormat formatter = new DecimalFormat("###,###,###");
    RichPanelGridLayout panelGridLayout = new RichPanelGridLayout();
    RichButton loadBtn = new RichButton();
    RichButton cancleBtn = new RichButton();
    RichButton confirmBtn = new RichButton();
    RichLink linkStatus = new RichLink();
    RichMessages message = new RichMessages();
    FacesMessage newMsg;
    FacesContext fc;
    List<String> msglst = new ArrayList<>();
    List<FacesMessage> newMsglst = new ArrayList<>();
    private final int[] positions_start = {0,16,28,36,54,72};
    private final int[] positions_end = {16,28,36,54,72,100};
    
    private final int[] index_map = {0,1,3,4,5};

    public void setPanelGridLayout(RichPanelGridLayout panelGridLayout) {
        this.panelGridLayout = panelGridLayout;
    }

    public RichPanelGridLayout getPanelGridLayout() {
        return panelGridLayout;
    }

    public void setLoadBtn(RichButton loadBtn) {
        this.loadBtn = loadBtn;
    }

    public RichButton getLoadBtn() {
        return loadBtn;
    }

    public void setCancleBtn(RichButton clearBtn) {
        this.cancleBtn = clearBtn;
    }

    public RichButton getCancleBtn() {
        return cancleBtn;
    }

    public void setConfirmBtn(RichButton confirmBtn) {
        this.confirmBtn = confirmBtn;
    }

    public RichButton getConfirmBtn() {
        return confirmBtn;
    }


    public void setLinkStatus(RichLink linkStatus) {
        this.linkStatus = linkStatus;
    }

    public RichLink getLinkStatus() {
        return linkStatus;
    }
    
    public void setMessage(RichMessages message) {
        this.message = message;
    }

    public RichMessages getMessage() {
        return message;
    }
    public void refreshComponent(){
        AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
        AdfFacesContext.getCurrentInstance().addPartialTarget(loadBtn);
        AdfFacesContext.getCurrentInstance().addPartialTarget(confirmBtn);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cancleBtn);
        AdfFacesContext.getCurrentInstance().addPartialTarget(linkStatus);
    }
    public void loadTargetData(){
        tableData.clear();
    }
    public void loadSourceData(){
        tableData.clear();
        String sqlData = "select * from MD_TBL_TEXT_IMDSSTATUS2  where rownum < 1000 ";
        List list = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForList(sqlData);
        tableData.addAll(list);
    }
    public void loadDataOnPage(java.math.BigDecimal processStatusId){
        if(processStatusId.intValue()==0 
            || processStatusId.intValue()==1
            || processStatusId.intValue()==2 
            || processStatusId.intValue()==3 ){
            loadSourceData();
        }else{
            loadTargetData();
        }
    }
    public void setVisibleFalseOnPage(){
        loadBtn.setVisible(false);
        cancleBtn.setVisible(false);
        confirmBtn.setVisible(false);
        linkStatus.setVisible(false);
        panelGridLayout.setVisible(false);
    }
    public void setVisibleOnPage(java.math.BigDecimal processStatusId){
        loadBtn.setVisible(false);
        cancleBtn.setVisible(false);
        confirmBtn.setVisible(false);
        linkStatus.setVisible(false);
        if(processStatusId.intValue()==0){ // noting
            loadBtn.setVisible(true);
        }else if(processStatusId.intValue()==2){ // Load Success
            cancleBtn.setVisible(true);
            confirmBtn.setVisible(true);
            linkStatus.setVisible(true);
        }else if(processStatusId.intValue()==3){ // Load Error
            cancleBtn.setVisible(true);
            linkStatus.setVisible(true);
        }else if(processStatusId.intValue()==5){ // Import Success
            loadBtn.setVisible(true);
            linkStatus.setVisible(true);
        }else if(processStatusId.intValue()==6){ // Import Error
            loadBtn.setVisible(true);
            linkStatus.setVisible(true);
        }else{
            linkStatus.setVisible(true);
        }
        
        linkStatus.setVisible(false);
    }    
    public java.math.BigDecimal coutTargetData(){
        Map map = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForMap("select count(1) as COUNT from MD_TBL_PART_STATUS2 ");
        java.math.BigDecimal count=(java.math.BigDecimal)map.get("COUNT");
        return count;
    }
    public java.math.BigDecimal coutSourceData(){
        Map map = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForMap("select count(1) as COUNT from MD_TBL_TEXT_IMDSSTATUS2 ");
        java.math.BigDecimal count=(java.math.BigDecimal)map.get("COUNT");
        return count;
    }         
    public void displayMappingMessage(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mapping Process running", null));    
    }
    public void displayMessage(java.math.BigDecimal processStatusId){
        if(processStatusId!=null ){
            if(processStatusId.intValue()==5){
                java.math.BigDecimal count = coutTargetData();
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Complete: "+formatter.format(count)+" Records, Error 0 Record");
                
                FacesContext.getCurrentInstance().addMessage(null, msg1);
        
            }else if(processStatusId.intValue()==0){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Load: Data is show detail before import to Database", null));    
            }else if(processStatusId.intValue()==1){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Loading", null));    
            }else if(processStatusId.intValue()==2){
                java.math.BigDecimal count = coutSourceData();
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Load Complete: "+formatter.format(count)+" Records, Error 0 Record");
                FacesMessage msg2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Confirm: This System import Data to Database");
                FacesContext.getCurrentInstance().addMessage(null, msg1);
                FacesContext.getCurrentInstance().addMessage(null, msg2);
            }else if(processStatusId.intValue()==3){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Load Error", null));    
            }else if(processStatusId.intValue()==4){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Importing", null));    
            }else if(processStatusId.intValue()==6){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Import Error", null));    
            }
        
    }
        
    }
    public void loadPage(){
        panelGridLayout.setVisible(true);
        config.bypassAccessRight = true;
        config.process.processButtonName = "Process All";
        config.process.processSingleRow = true;
        
        newMsg =  new FacesMessage();
        message.setInline(true);  
        // Clear Message
        IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 
        java.math.BigDecimal processStatusId = null;
        String sqlControl = "select * from MD_TBL_CONTROL_MAPPING  ";
        Map<String, Object> mapping_status= RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForMap(sqlControl.toString());
        if(mapping_status!=null){
            BigDecimal mappingStatusId = (BigDecimal)mapping_status.get("PROCESS_STATUS_ID");
            log("mappingStatusId-->"+mappingStatusId);
            //if(mappingStatusId!=null && ( mappingStatusId.intValue()==2 || mappingStatusId.intValue()==4 )){
                if(mappingStatusId!=null && ( mappingStatusId.intValue()!=1  && mappingStatusId.intValue()!=0 )){
                sqlControl = "select * from MD_TBL_CONTROL_IMPORT where PROCESS_ID = ? ";
                        String processID = "IMPORT_IMDS_STATUS";
                        List list = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForList(sqlControl.toString(), new Object[] { processID });
                        if(list!=null && list.size()>0){
                            Map map = (Map)list.get(0);
                            processStatusId=(java.math.BigDecimal)map.get("PROCESS_STATUS_ID");
                            log("PROCESS_STATUS_ID=["+processStatusId+"]");
                            // set button 
                            setVisibleOnPage(processStatusId);
                            loadDataOnPage(processStatusId);
                            displayMessage(processStatusId); 
                        }else{
                            setVisibleFalseOnPage();
                            displayMappingMessage(); 
                        }  
            }else{
               setVisibleFalseOnPage();
               displayMappingMessage(); 
           }
        }
          
    }
    @Override
    public void preRender(ComponentSystemEvent componentSystemEvent) {
        // TODO Implement this method
        super.preRender(componentSystemEvent);
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap();
        if(requestMap.get("status")!=null&&requestMap.get("status").equals("S")){
            loadBtn.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(findUIComponent("load_btn"));
            if(!tableData.isEmpty()||tableData!=null){
                tableData.clear();
            }
            
            message.setInline(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(message);
            // Clear Message
            IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 

        }
    }

    public MD0004P01() {
        super();
    }

    @Override
    public boolean onProcess(List<Map<String, Object>> list) {
        // TODO Implement this method
        return false;
    }

    @Override
    protected String getTableSQL() {
        // TODO Implement this method
        //return "MD_TBL_PROCESS_LOG";
        return null;
    }

    @Override
    public void init(RzInitialConfig rzInitialConfig) {
        // TODO Implement this method
        loadPage();
    }
    
    public String onClickFirst(){
        rzTable.setFirst(0);
        loadPage();
        refreshComponent();
        return "";
    }
    public String onClickPrevious(){
        int first = rzTable.getFirst();
        int fetchSize =  rzTable.getFetchSize();
        int size = ((List)rzTable.getValue()).size();
        log(" into onClickPrevious first["+first+"] , fetchSize["+fetchSize+"] , size["+size+"]");
        rzTable.setFirst(first-fetchSize);
       
        loadPage();
        refreshComponent();
       
        return "";
    }
    public String onClickNext(){
        int first = rzTable.getFirst();
        int fetchSize =  rzTable.getFetchSize();
        int size = ((List)rzTable.getValue()).size();
        log(" into onClickNext first["+first+"] , fetchSize["+fetchSize+"] , size["+size+"]");
        rzTable.setFirst(first+fetchSize);
       
        loadPage();
        refreshComponent();
       
        return "";
    }
    public String onClickLast(){
        int first = rzTable.getFirst();
        int fetchSize =  rzTable.getFetchSize();
        int size = ((List)rzTable.getValue()).size();
        log(" into onClickLast first["+first+"] , fetchSize["+fetchSize+"] , size["+size+"]");
        int lastPage = size/fetchSize;
        log(lastPage);
        rzTable.setFirst(lastPage*50);
       
        loadPage();
        refreshComponent();
       
        return "";
    }
    public void onTableRangeChange(RangeChangeEvent event) {
        startPagingIndex = event.getNewStart() + 1;
        endPagingIndex = event.getNewEnd() + 1;


        ELog.log("onTableRangeChange");
        ELog.log("startPagingIndex : " + startPagingIndex);
        ELog.log("endPagingIndex : " + endPagingIndex);

    }
    public final void onClickLoad(ActionEvent event) {
        
        ELog.trace(programCode, userInfo, "onClickLoad - Start");
      
        try {
            ServletContext con = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
           
            InterFaceFileModel interFaceFileModel= IMDSServices.INSTANCE.getInterFaceFileModel("IMDS");
            BigDecimal statusId=null;
            if(interFaceFileModel!=null){

                String localFilePath = con.getRealPath("/data")+ File.separatorChar ;//+"part_supplier_bk.txt";
                /* 
                File file1=new File("/Users/imake/WORK/PROJECT/IMDS/03 New requirement/Interface file_V3/IMDS_INPUT_L6E641.TXT");
               // File file2=new File("/Users/imake/WORK/PROJECT/IMDS/03 New requirement/Interface file_V3/IMDS_INPUT_L6F605.TXT");
                List<File> files = new ArrayList<File>(1);//IMDSServices.INSTANCE.loadFile(interFaceFileModel,localFilePath);//,"IMDS_INPUT_");
                files.add(file1);
               // files.add(file2);
                */
                 List<File> files = IMDSServices.INSTANCE.loadFile(interFaceFileModel,localFilePath);//,"IMDS_INPUT_");
                log("files=>"+files);
                
                if(files!=null && files.size()>0){
                    String updateStatusSql = "update MD_TBL_CONTROL_IMPORT set PROCESS_STATUS_ID=1 where PROCESS_ID='IMPORT_IMDS_STATUS' ";
                    RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
                    
                    int result = IMDSServices.INSTANCE.truncatTable("TRUNCATE TABLE MD_TBL_TEXT_IMDSSTATUS2 "); 
                    log("result --> "+result);
                    
                    Map indexMap = new HashMap();
                    for(int i=0;i<index_map.length;i++){
                        indexMap.put(i+3, index_map[i]);
                    }
                    String sqlImport = "INSERT INTO MD_TBL_TEXT_IMDSSTATUS2 " +
                        "(RECORD_ID,RECORD_SEQ, PART_NO,SUPPLIER_ID,NODE_ID,MODULE_ID,VERSION_NO ) " +
                        "VALUES (?,?,?,?,?,?,?)" ;
                    ImportModel impportModel = new ImportModel();
                    for(File file : files){
                        ImportModel impport =  IMDSServices.INSTANCE.loadDataFromFile(file,positions_start,positions_end);
                        if(impport.getDataList()!=null && impport.getDataList().size()>0)
                            impportModel.getDataList().addAll(impport.getDataList());
                       // file.delete();
                    }
                    
                    IMDSServices.INSTANCE.importData(impportModel,sqlImport,indexMap);
                    
                    //file.delete();
                    updateStatusSql = "update MD_TBL_CONTROL_IMPORT set PROCESS_STATUS_ID=2 where PROCESS_ID='IMPORT_IMDS_STATUS' ";
                            RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
                    statusId=BigDecimal.valueOf(2);
                }
                
            }
           
            loadPage();
            // Clear Message
            IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 
            if(statusId!=null){
                displayMessage(statusId);
            }else{
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Can't find IMDS input Status File");
                FacesMessage msg2 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please contact Administrator for check config Interface file");
                FacesContext.getCurrentInstance().addMessage(null, msg1);
                FacesContext.getCurrentInstance().addMessage(null, msg2);
            }
            refreshComponent();
            ELog.trace(programCode, userInfo, "onClickLoad - finish");
        } catch (Exception e) {
            log("ERORRRRR");
            FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Incorrect config Interface file ( Username or Password of FTP Server )");
            FacesMessage msg2 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please contact Administrator for check config Interface file");
            FacesContext.getCurrentInstance().addMessage(null, msg1);
            FacesContext.getCurrentInstance().addMessage(null, msg2);
        }

    }
    
    
    public final void onClickCancel(ActionEvent event) {
        
        ELog.trace(programCode, userInfo, "onClickCancel - Start");
        int result = IMDSServices.INSTANCE.truncatTable("TRUNCATE TABLE MD_TBL_TEXT_IMDSSTATUS2 "); 
        log("result --> "+result);
        IMDSServices.INSTANCE.truncatTable("TRUNCATE TABLE MD_TBL_PART_STATUS2 "); 
        
        String updateStatusSql = "update MD_TBL_CONTROL_IMPORT set PROCESS_STATUS_ID=0 where PROCESS_ID='IMPORT_IMDS_STATUS' ";
        RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
        try {
            loadPage();
            // Clear Message
            IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 
            displayMessage(BigDecimal.valueOf(0));
            
            refreshComponent();
            ELog.trace(programCode, userInfo, "onClickCancel - finish");
        } catch (Exception e) {
            
            showError(e);
        }

    }
    
    
    public final void onClickConfirm(ActionEvent event) {
        
        ELog.trace(programCode, userInfo, "onClickConfirm - Start");
        String updateStatusSql = "update MD_TBL_CONTROL_IMPORT set PROCESS_STATUS_ID=4 where PROCESS_ID='IMPORT_IMDS_STATUS' ";
        RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
        
        String storeCall = "{call MD_STP_IMP_PART_STATUS(?,?)}";
        String[] storeParams = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
        IMDSServices.INSTANCE.callSP(storeCall,storeParams); 
        
        updateStatusSql = " update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=1 where PROCESS_ID='MAPPING' ";
               RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
        try {
            tableData.clear();
            loadPage();
            rzTable.setFirst(0);
            // Clear Message
            IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 
            displayMessage(BigDecimal.valueOf(5));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mapping Process running", null));    
            refreshComponent();
            
            
            
            String[] storeCalls = {"{call MD_STP_MAPPING_V2(?,?)}"
                                  ,"{call MD_STP_GEN_DUMMY(?,?)}" 
                                  ,"{call MD_STP_GEN_BY_SYS(?,?)}"
                                  ,"{call MD_STP_MAPPING_SUM(?,?)"
                                  ,"{call MD_STP_INPUT_STATUS(?,?)"};
            // UserInfo userInfo = RzUtils.INSTANCE.getUserInfoFromSession();
            // String[] storeParams = {userInfo.getUserName(),userInfo.getHostName()};
            String[] storeParamsList = {"MAAPPING_SYSTEM",""};
            MappingMultipleRunner runner = 
                       new MappingMultipleRunner(storeCalls,storeParamsList);
                 Thread importThread  = new Thread(runner);
            
            importThread.start();
            System.out.println("Mapping Runner name["+importThread.getName()+"]"+(new Date()));
            
            ELog.trace(programCode, userInfo, "onClickConfirm - finish");
        } catch (Exception e) {
            
            showError(e);
        }

    }
    public void pollListenerActive(PollEvent pollEvent) {
            Random rand = new Random();

            int  n = rand.nextInt(50) + 1;
            log(" in to pollListenerActive ["+n+"]");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, n+"", null));
        }

}
