package com.mmth.imds.backing.app;

import com.mmth.framework.util.ELog;
import com.mmth.framework.util.EStringUtils;
import com.mmth.framework.util.RzJDBCUtils;
import com.mmth.imds.model.process.ImportModel;
import com.mmth.imds.model.process.MappingStatusModel;
import com.mmth.imds.service.IMDSServices;
import com.mmth.imds.threads.ImportMultipleRunner;
import com.mmth.imds.threads.ImportRunner;
import com.mmth.imds.threads.MappingMultipleRunner;
import com.mmth.mdf.controller.config.RzInitialConfig;
import com.mmth.mdf.controller.template.MDFTAPP01;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.util.ResetUtils;

import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.UploadedFile;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class MD0006P01 extends MDFTAPP01{
    DecimalFormat formatter = new DecimalFormat("###,###,###");
    DecimalFormat formatter_with_zero = new DecimalFormat("0.00");
    RichButton editButtonBind = new RichButton();
    RichButton dummyButtonBind = new RichButton();
    RichInputFile inputFileBind =new RichInputFile();
    public boolean editMode;
    RichTable t1 = new RichTable();
    List<Map<String, Object>> tableData1 = new ArrayList<>();
    public RichPopup popupEdit = new RichPopup();
    public Map<String, Object> editRow = new HashMap<>(); //  edit record
    protected Map<String, Object> beforeEditRow; //  Before edit record

    public void setEditButtonBind(RichButton editButtonBind) {
        this.editButtonBind = editButtonBind;
    }

    public RichButton getEditButtonBind() {
        return editButtonBind;
    }

    public void setDummyButtonBind(RichButton dummyButtonBind) {
        this.dummyButtonBind = dummyButtonBind;
    }

    public RichButton getDummyButtonBind() {
        return dummyButtonBind;
    }

    public void setInputFileBind(RichInputFile inputFileBind) {
        this.inputFileBind = inputFileBind;
    }

    public RichInputFile getInputFileBind() {
        return inputFileBind;
    }

    public void setEditRow(Map<String, Object> editRow) {
        this.editRow = editRow;
    }

    public Map<String, Object> getEditRow() {
        return editRow;
    }

    public void setBeforeEditRow(Map<String, Object> beforeEditRow) {
        this.beforeEditRow = beforeEditRow;
    }

    public Map<String, Object> getBeforeEditRow() {
        return beforeEditRow;
    }

    public void setPopupEdit(RichPopup popupEdit) {
        this.popupEdit = popupEdit;
    }

    public RichPopup getPopupEdit() {
        return popupEdit;
    }

    public void setT1(RichTable t1) {
        this.t1 = t1;
    }

    public RichTable getT1() {
        return t1;
    }

    public MD0006P01() {
        super();
    }
    
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isEditMode() {
        return editMode;
    }
    
    public final boolean getRenderAdd() {
        return canAdd && !editMode;
    }

    public boolean getRenderEdit() {
        return (canEdit && tableData.size() > 0) && !editMode;
    }

    public boolean getRenderDelete() {
        return (canDelete && tableData.size() > 0) && !editMode;
    }

    public boolean getRenderProcess() {
        return canProcess && !editMode;
    }

    public boolean getDisableAdd() {
        return !getRenderAdd();
    }

    public boolean getDisableEdit() {
        return !getRenderEdit();
    }

    public boolean getDisableDelete() {
        return !getRenderDelete();
    }

    public boolean getDisableProcess() {
        return !getRenderProcess();
    }

    @Override
    public boolean getDisableSearch() {
        return !(canSearch && !editMode);
    }
    
    
    @Override
    public boolean onProcess(List<Map<String, Object>> list) {
        // TODO Implement this method
        return false;
    }
    
    public String[] getPKColumn() {
        return new String[]{"SUPPLIER_CODE","UPG_CODE","SERIES_NO","PART_NO","MAPPING_TYPE"};
    }
    
    public String getTableName() {
        return "MD_TBL_PART_SUM";
    }
 
    @Override
    protected String getTableSQL() {
        // TODO Implement this method
       return "SELECT * FROM MD_TBL_PART_SUM ORDER BY MC_CODE ASC , ORDER_ID ASC ";
    }

    @Override
    public void init(RzInitialConfig rzInitialConfig) {
        // TODO Implement this method
        config.bypassAccessRight = true;
        config.process.processButtonName = "Process All";
        config.process.processSingleRow = true;       
        
        config.export.excelColumnDB = new String[]{"MC_CODE","PART_NO","SUPPLIER_CODE","UPG_CODE","LEVEL_NO","EPL_PART_NO","P_NAME","QTY","LOCAL_P_NO","ORDER_P_NO","MAPPING_TYPE",
                                                   "ORDER_ID","PARENT_PART"};
        config.export.excelHeader = config.export.excelColumnDB ;
        config.export.usePOI = true;
        getTableSQL();
        setPropertyForRzTableOne(t1);
        initData();
    }
    public  void onClickRefreshData(ActionEvent event) {
        initData();
    }
    
    protected RichTable setPropertyForRzTableOne(RichTable t1){
                
        t1.setVar("rows");
        t1.setContentDelivery("immediate");
        t1.setRowBandingInterval(1);
        t1.setValue(tableData1);
        t1.setSummary("Sum");
        t1.setColumnStretching("last");
        t1.setColumnSelection("none");
        t1.setColumnResizing("enabled"); // disabled
        t1.setScrollPolicy("page");
        t1.setDisableColumnReordering(true);
        t1.setAllDetailsEnabled(false);
        t1.setStyleClass("AFStretchWidth");
        t1.setFirst(startPagingIndex - 1);

        return t1;
    }
    public void checkVisible(){
        editButtonBind.setVisible(true);
        dummyButtonBind.setVisible(true);
        inputFileBind.setVisible(true); 
        String sqlControl = "select * from MD_TBL_CONTROL_MAPPING  ";
        Map<String, Object> mapping_status= RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForMap(sqlControl.toString());
        if(mapping_status!=null){
            BigDecimal mappingStatusId = (BigDecimal)mapping_status.get("PROCESS_STATUS_ID");
            log("mappingStatusId-->"+mappingStatusId);
            if(mappingStatusId!=null && mappingStatusId.intValue()!=2){
                editButtonBind.setVisible(false);
                dummyButtonBind.setVisible(false);
                inputFileBind.setVisible(false); 
            }
        }
        if(rzTable!=null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
        if(getToolbar()!=null)
            AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
        if(getTotalBar()!=null)
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
    }
    public  java.math.BigDecimal totalMappingRecord(){
        Map map = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForMap("select count(1) as COUNT from MD_TBL_PART_SUM ");
        java.math.BigDecimal count=(java.math.BigDecimal)map.get("COUNT");
        return count;
    }
    
    public MappingStatusModel calculateMappingStatusModel(String mappingType,String mappingDesc, java.math.BigDecimal count){
        String sqlData ="";
        String mappingTypeWhere="'"+mappingType+"'";
        if(mappingType.equals("2")){
            mappingTypeWhere ="'"+mappingType+"'"+",'9'";
        }
            sqlData = "select " +
              //" MAPPING_TYPE  , "+
                      /*
              " CASE MAPPING_TYPE "+
              " WHEN '1' THEN 'KD ID Replace' "+
              " WHEN '2' THEN 'Actual Mapping' "+
              " WHEN '3' THEN 'Suffix Replace' "+
              " WHEN '4' THEN 'System Generate' "+
              " WHEN '5' THEN 'Not found supplier' "+
              " WHEN '6' THEN 'Generate Dummy' "+
              " ELSE '' "+
             " END as MAPPING_NAME, "+
              */
              " count(MAPPING_TYPE) as MAPPING_RECORD ,  ( count(MAPPING_TYPE) * 100 )/"+count.intValue()+" , "+
              " Round(( count(MAPPING_TYPE) * 100 )/"+count.intValue()+" , 2) as MAPPING_PERCENT "+
             " from MD_TBL_PART_SUM "+
             " where MAPPING_TYPE in ("+mappingTypeWhere+") ";
             //" GROUP BY  MAPPING_TYPE  ";
        
    
        MappingStatusModel mappingStatusModel = new MappingStatusModel();
        String status = mappingType;
        String description = mappingDesc;
        String record = "0";
        String percent = "0%";
        List list = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForList(sqlData);
        if(list!=null && list.size()>0){
            for(int i=0;i<list.size();i++){
               Map mapObject =  (Map)list.get(i);
              record = mapObject.get("MAPPING_RECORD")!=null?formatter.format((java.math.BigDecimal)mapObject.get("MAPPING_RECORD")):"";
                Object mappPercentObject = mapObject.get("MAPPING_PERCENT");
                if(mappPercentObject!=null){
                   java.math.BigDecimal mappPercent = (java.math.BigDecimal)mappPercentObject;
                    if(String.valueOf(mappPercent).indexOf(".")!=-1){
                        percent = formatter_with_zero.format(mappPercent);
                    }else
                        percent = formatter.format(mappPercent);
                    percent = percent+"%";
                }     
            }
        }
       
        mappingStatusModel.setStatus(status);
        mappingStatusModel.setDescription(description);
        mappingStatusModel.setRecord(record);
        mappingStatusModel.setPercent(percent);
           
         return mappingStatusModel;  
    }
    public final void initData() {
        
        ELog.trace(programCode, userInfo, "onClickSearch - Start");
        
        try {

                ELog.log("screenSQL : " + screenSQL);
    
                t1.setFirst(0);
                startPagingIndex = 0;
                endPagingIndex = t1.getFetchSize();
            
                t1.setValue(tableData1);
                tableData1.clear();
                Map<String,Object> data;
                List<Map<String,Object>> datalst = new ArrayList<>();
            String[] mappingTypes = {"1","2","3","4","5","6"};
             String[] mappingNames = {"Actual Mapping","Generate Dummy","Suffix Replace","System Generate","Not found supplier"
                                      ,"Update Error"};
           // String[] mappingNames = {"KD ID Replace","Actual Mapping","Suffix Replace","System Generate","Not found supplier"
             //                        ,"Generate Dummy"};
            
           java.math.BigDecimal count = totalMappingRecord();
            for(int i =0 ;i < mappingTypes.length;i++){
                MappingStatusModel mappingStatusModel= calculateMappingStatusModel(mappingTypes[i],mappingNames[i],count);
                data = new HashMap<>();
                data.put("STATUS", mappingStatusModel.getStatus());
                data.put("DESCRIPTION", mappingStatusModel.getDescription());
                data.put("RECORD", mappingStatusModel.getRecord());
                data.put("PERCENT", mappingStatusModel.getPercent());
                
                datalst.add(data);
            }
            
                tableData1.addAll(datalst);
                
                AdfFacesContext.getCurrentInstance().addPartialTarget(t1);
           
            String mappingControlSQL = " select * from MD_TBL_CONTROL_MAPPING where PROCESS_ID='MAPPING' ";
            List resultControl = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForList(mappingControlSQL);
            if(resultControl!=null && resultControl.size()>0){
                Map map = (Map)resultControl.get(0);
                BigDecimal processStatusId = (BigDecimal)map.get("PROCESS_STATUS_ID");
                int status = processStatusId.intValue();
                IMDSServices.INSTANCE.clearMessage(FacesContext.getCurrentInstance()); 
                /*
                    0=wait for mapping
                    1=process mapping
                    2=mapping success
                    3=mapping error
                */
               
                String message = "";
                boolean isError = false;
                if(status==0){
                    message = "Wait for Mapping ";
                }else if(status==1){
                    message = "Processing Mapping ";
                }else if(status==2){
                    message = "Mapping Success";
                }else if(status==3){
                    message = "Mapping Error";
                    isError=true;
                }else if(status==4){
                    message = "Wait for Load all Interface file";
                }
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
                if(isError)
                    msg1.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, msg1);
                
            }
            checkVisible();
            ELog.trace(programCode, userInfo, "onClickSearch - finish");
        } catch (Exception e) {
            
            showError(e);
        }
    }
    
    
    public final void onClickEdit(ActionEvent event) {
        try {
            if (this.getPKColumn().length > 0) {     
                
                    RowKeySet editRowList = this.rzTable.getSelectedRowKeys();
                   
                    if(editRowList != null && editRowList.size() == 1){
                   
                        editRow = (Map<String, Object>) this.rzTable.getSelectedRowData();
    
                        if (validateBeforeEdit(editRow)) {
                            beforeEditRow = new LinkedHashMap<String, Object>(editRow);
                            beforeEdit(beforeEditRow, editRow);
    
                            RichPopup.PopupHints hints = new RichPopup.PopupHints();
                            hints.add(RichPopup.PopupHints.HintTypes.HINT_ALIGN, RichPopup.PopupHints.AlignTypes.ALIGN_OVERLAP);
                            popupEdit.show(hints);
                        }
                    }else{
                        showError("Please select only one record."); 
                    }
                
            } else {
                showError("[PROGRAMMER BUG] Please set getPKColumn().");
            }
        } catch (Exception e) {
            showError(e);
        }
    }
    
    public final boolean processUpdate(Map<String, Object> beforeEditRow, Map<String, Object> editRow) throws SQLException {
        ELog.log(" ON Process Update : ");

        if (editRow.keySet().size() > 0) {

            StringBuilder firstSectionSQL = new StringBuilder("UPDATE ").append(getTableName()).append(" SET ");
            StringBuilder lastSectionSQL = new StringBuilder(", "+config.master.columnNameUpdatedDate+" = SYSTIMESTAMP , "+config.master.columnNameUpdatedBy+" = ? WHERE ");
            List<Object> updateParam = new LinkedList<Object>();

            Map<String, Object> updateList = getUpdateColumn(beforeEditRow, editRow);

            ELog.log(" updateList() : " + updateList);
            int i = 1;
            for (Map.Entry<String, Object> column : updateList.entrySet()) {

                if (column.getValue() == null) {
                    firstSectionSQL.append(column.getKey()).append(" = NULL ,");
                } else {
                    firstSectionSQL.append(column.getKey()).append(" = ? ,");
                    updateParam.add(column.getValue());
                }
                i++;
                ELog.log(" firstSectionSQL() " + i + " : " + firstSectionSQL);
            }

            updateParam.add(userInfo.getUserName());

            ELog.log(" getPKColumn() : " + Collections.singletonList(getPKColumn()));
            
            for (String pk : getPKColumn()) {
                lastSectionSQL.append(" ").append(pk).append(" = ? AND");
                updateParam.add(beforeEditRow.get(pk));
            }

            firstSectionSQL.deleteCharAt(firstSectionSQL.length() - 1);

            firstSectionSQL.append(EStringUtils.INSTANCE.substringBeforeLast(lastSectionSQL.toString(), "AND"));

            int rowEffect = 0;
            
            SingleConnectionDataSource sds  = new SingleConnectionDataSource(RzJDBCUtils.INSTANCE.getJDBCTemplate().getDataSource().getConnection(),true);            
            JdbcTemplate template = new JdbcTemplate(sds);
            template.getDataSource().getConnection().setAutoCommit(false);
            List<String> extraSQLList = new ArrayList<String>();
            int[] result = null;

            if (config.master.processBeforeUpdate) {
                processBeforeUpdate(editRow, extraSQLList); //<------

                if (extraSQLList.size() > 0) {
                    result = template.batchUpdate(extraSQLList.toArray(new String[0]));

                    if (checkResultBeforeProcessUpdate(extraSQLList, result)) {
                        //template.getDataSource().getConnection().commit();
                    } else {
                        template.getDataSource().getConnection().rollback();
                        return false;
                    }
                }

            }

            if(!config.master.processAfterUpdate){            
                rowEffect = RzJDBCUtils.INSTANCE.update(firstSectionSQL.toString(), updateParam.toArray());
                template.getDataSource().getConnection().commit();
            } else {
                               
                    try {
                        
                        rowEffect = template.update(firstSectionSQL.toString(), updateParam.toArray());

                        processAfterUpdate(beforeEditRow, editRow,extraSQLList);
                        
                        if(extraSQLList.size() >0){
                           result = template.batchUpdate(extraSQLList.toArray(new String[0]));
                           
                           if(checkResultAfterProcessUpdate(extraSQLList,result)){
                               template.getDataSource().getConnection().commit();   
                           } else {
                               template.getDataSource().getConnection().rollback();
                           }
                        }
                        
                    } catch(Exception e){
                        showError(e);
                        template.getDataSource().getConnection().rollback();
                    }finally{
                        template.getDataSource().getConnection().close();
                        sds.destroy();
                    }            
            }
            
            ELog.log("Row Effect : " + rowEffect);

            return (rowEffect != 0);

        } else {
            showError("[PROGRAMER BUG] Not Any Assign Create Field.");
        }

        return true;
    }
    
    
    protected Map<String, Object> getUpdateColumn(Map<String, Object> beforeEdit, Map<String, Object> editRow) {
        
        Map<String, Object> updateList = new LinkedHashMap<String, Object>();
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, Object> updateColumn : editRow.entrySet()) {
            ELog.log("beforeEdit [K]: "+beforeEdit.get(updateColumn.getKey())+" beforeEdit [V]: "+beforeEdit.get(updateColumn.getValue()));
            ELog.log("editRow [K]: "+editRow.get(updateColumn.getKey())+" editRow [V]: "+editRow.get(updateColumn.getValue()));
            ELog.log("updateColumn [K]: "+updateColumn.getKey()+" updateColumn [V}: "+updateColumn.getValue());
            //--- Check Update Row ---
            if (beforeEdit.get(updateColumn.getKey()) == null && editRow.get(updateColumn.getKey()) == null) {
                continue; // SKIP NO UPDATE FIELD
            }
            if (EStringUtils.INSTANCE.startsWith(updateColumn.getKey(), "#")) {
                continue; // SKIP THIS FIELD
            }

            if (beforeEdit.get(updateColumn.getKey()) == null) {
                updateList.put(updateColumn.getKey(), updateColumn.getValue());
                sb.append("["+updateColumn.getKey()+":null=>"+updateColumn.getValue()+"]");
            } else if (editRow.get(updateColumn.getKey()) == null) {
                updateList.put(updateColumn.getKey(), null);
                sb.append("["+updateColumn.getKey()+":"+updateColumn.getValue()+"=>null]");
            } else if (EStringUtils.INSTANCE.equals(beforeEdit.get(updateColumn.getKey()).toString(), editRow.get(updateColumn.getKey()).toString())) {
                continue; // SKIP NO UPDATE FIELD
            } else {

                sb.append("["+updateColumn.getKey()+":"+ beforeEdit.get(updateColumn.getKey())+"=>"+updateColumn.getValue()+"]");
                print(updateColumn.getKey() + " from " + beforeEdit.get(updateColumn.getKey()) + " ===> " + updateColumn.getValue());
                
                if(  beforeEdit.get(updateColumn.getKey()) instanceof java.util.Date){
                    
                    if( ((Date)beforeEdit.get(updateColumn.getKey())).getTime() !=  ((Date)editRow.get(updateColumn.getKey())).getTime() ){
                        updateList.put(updateColumn.getKey(), updateColumn.getValue());        
                    }
                    
                } else {
                    updateList.put(updateColumn.getKey(), updateColumn.getValue());    
                }
                
                
            }
        }
        print("Edit Column Size : "+updateList);
        
        ELog.info(programCode, userInfo, sb.toString());
        
        return updateList;
    }
    public final void onClickUpdateAndCallSP(ActionEvent event) {

        ELog.log("=== On Click Update ==");
         
        if ( haveUpdateColumn(beforeEditRow, editRow)) {
            if (validateBeforeUpdate(beforeEditRow, editRow)) {
                try {

                    beforeUpdate(beforeEditRow, editRow);
                    String  updateStatusSql = " update MD_TBL_PART_REPORT9 set SUPPLIER_CODE='"+editRow.get("SUPPLIER_CODE")+"' ,ORDER_P_NO='"+editRow.get("PARENT_PART")+"'  " +
                                            " where PARENT_PART='"+editRow.get("PARENT_PART")+"' " +
                                            " and LEVEL_NO !=1 and SUPPLIER_CODE='-' ";
                        
                        
                    RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql); 
                    
                        updateStatusSql = " update MD_TBL_PART_REPORT9 set SUPPLIER_CODE='"+editRow.get("SUPPLIER_CODE")+"' " +
                            " where PARENT_PART='"+editRow.get("PARENT_PART")+"' " +
                            " and LEVEL_NO=1 "; 
                     RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
                    
                    updateStatusSql = " update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID='1' " +
                        " where PROCESS_ID='MAPPING' ";
                    RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
                    
                    
                     updateStatusSql = " update MD_TBL_PART_REPORT9 set MAPPING_TYPE='6' " +
                        " where PARENT_PART='"+editRow.get("PARENT_PART")+"' " +
                        //" and  ORDER_ID='"+editRow.get("ORDER_ID")+"' " +
                        " and  MC_CODE='"+editRow.get("MC_CODE")+"' ";
                    RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql);
                    log("Edit Row="+editRow);
                    editRow.put("MAPPING_TYPE", "6");
                    log("CALL SP MD_STP_GEN_DUMMY_SUPP="+editRow.get("ORDER_ID")+" , "+editRow.get("PARENT_PART")+" , "+editRow.get("MC_CODE"));
                   
                    String[] storeCall = {"{call MD_STP_GEN_DUMMY_SUPP(?,?,?)}","{call MD_STP_MAPPING_SUM(?,?)","{call MD_STP_INPUT_STATUS(?,?)"};
                    //String storeCall = "{call MD_STP_GEN_DUMMY_SUPP(?,?,?)}";
                    String[] storeParams1 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress(),(String)editRow.get("PARENT_PART")};
                    String[] storeParams2 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    String[] storeParams3 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    List<String[]> paramList = new ArrayList<String[]>();
                    paramList.add(storeParams1);
                    paramList.add(storeParams2);
                    paramList.add(storeParams3);
                    ImportMultipleRunner runner = 
                               new ImportMultipleRunner(storeCall,paramList);
                         Thread importThread  = new Thread(runner);
                    
                    importThread.start();
                    log("ImportRunner name["+importThread.getName()+"]");
                    
                    if (config.searchAfterUpdate) {
                        this.onClickSearch(event);
                    }
                     initData();
                     AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                     AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                     AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                    /*
                    if (processUpdate(beforeEditRow, editRow)) {
                       
                        showInfo("Update Successfully.");

                        if (config.searchAfterUpdate) {
                            this.onClickSearch(event);
                        }
                         initData();
                         AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                         AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                         AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                    } else {
                        showError("No row effect records ?");
                    }
                    */
                } catch (Exception e) {
                    showError(e);
                }

                popupEdit.cancel();
            }
        } else {
            showError("Please update data.");
        }
        //showInfo("Processing Mapping");
    }
    
    public final void onClickUpdate(ActionEvent event) {

        ELog.log("=== On Click Update ==");
         
        if ( haveUpdateColumn(beforeEditRow, editRow)) {
            if (validateBeforeUpdate(beforeEditRow, editRow)) {
                try {

                    beforeUpdate(beforeEditRow, editRow);
                    editRow.put("MAPPING_TYPE", "6");
                    if (processUpdate(beforeEditRow, editRow)) {
                       
                        showInfo("Update Successfully.");

                        if (config.searchAfterUpdate) {
                            this.onClickSearch(event);
                        }
                         initData();
                         AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                         AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                         AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                    } else {
                        showError("No row effect records ?");
                    }

                } catch (Exception e) {
                    showError(e);
                }

                popupEdit.cancel();
            }
        } else {
            showError("Please update data.");
        }
    }
    
    
    protected boolean haveUpdateColumn(Map<String, Object> beforeEdit, Map<String, Object> editRow) {
        Map<String, Object> updateList = new LinkedHashMap<String, Object>();

        for (Map.Entry<String, Object> updateColumn : editRow.entrySet()) {

            //--- Check Update Row ---
            if (beforeEdit.get(updateColumn.getKey()) == null && editRow.get(updateColumn.getKey()) == null) {
                continue; // SKIP NO UPDATE FIELD
            }
            if (EStringUtils.INSTANCE.startsWith(updateColumn.getKey(), "#")) {
                continue; // SKIP THIS FIELD
            }

            if(beforeEdit.get(updateColumn.getKey())!=null&&editRow.get(updateColumn.getKey())!=null){
                
                 ELog.log("Not Null");

                if (EStringUtils.INSTANCE.equals(beforeEdit.get(updateColumn.getKey()).toString(), editRow.get(updateColumn.getKey()).toString())) {         
                    continue; // SKIP NO UPDATE FIELD
                }
            }
            
            return true;
        }

        return false;
    }
    
    
    public ArrayList getSupplierCode(){
        ELog.log("-----getConStructList----");
       
        ArrayList<SelectItem> list = new ArrayList<>();
        
        String sql = "SELECT SUPPLIER_ID,SUPPLIER_NAME_ENG FROM MD_TBL_SUPPLIER_MASTER";
        try {
            List<Map<String, Object>> dataList = query(sql);
            ELog.log("dataList: "+dataList);
            for(Map<String, Object> map: dataList){
                info("map : "+map);
                SelectItem selectItem = new SelectItem();
                selectItem.setValue(map.get("SUPPLIER_ID"));
                selectItem.setLabel(map.get("SUPPLIER_ID")+" : "+map.get("SUPPLIER_NAME_ENG").toString());
                list.add(selectItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
        
        return list;
    }
    

    public void onClickExportExcel(FacesContext fc, OutputStream out) {
        ELog.trace(programCode, userInfo, "onClickExportExcel - Start");
        processExport(out, true);
        ELog.trace(programCode, userInfo, "onClickExportExcel - Start");
    }
    
    public void uploadExcelUpdateErrorFile(ValueChangeEvent vce) {
//        int[] index_map = {0,1,2,3,4,5,6,7,8,9,10,11,12};
          int[] index_map = {2,3,4,5,7,8,12};
        Map indexMap = new HashMap();
        for(int i=0;i<index_map.length;i++){
            indexMap.put(i, index_map[i]);
        }
        //MC_CODE,PART_NO,SUPPLIER_CODE,UPG_CODE,LEVEL_NO,EPL_PART_NO,P_NAME
        //QTY,LOCAL_P_NO,ORDER_P_NO,MAPPING_TYPE,ORDER_ID,PARENT_PART

        String fileName="";
            if (vce.getNewValue() != null) {
                //Get File Object from VC Event
                UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                if (fileVal == null) {

                } else {
                InputStream inputStream = null;
                try {
                    // dump backup
                    String storeCallBack = "{call MD_STP_IMP_BACKUP(?,?)}";
                    String[] storeParams = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    IMDSServices.INSTANCE.callSP(storeCallBack,storeParams); 
                    
                    inputStream = fileVal.getInputStream();
                    List<String[]> dataList = IMDSServices.INSTANCE.extractXLS(inputStream, index_map);
                    log("show "+dataList.get(0)+" size="+dataList.get(0).length);
                   String sqlupdate = " update MD_TBL_PART_REPORT9 set SUPPLIER_CODE=? "+ 
                      ", UPDATE_FLAG='Y' , MAPPING_TYPE='6' "+
                    " WHERE  UPG_CODE = ? and LEVEL_NO=? "+
                     " and EPL_PART_NO=? and QTY=? "+
                      " and LOCAL_P_NO=? and PARENT_PART=? ";
                   
                    IMDSServices.INSTANCE.updateSupplierMapping(dataList,sqlupdate,indexMap);
                    
                    String[] storeCall = {"{call MD_STP_GEN_ALL_SUPP(?,?)}","{call MD_STP_MAPPING_SUM(?,?)","{call MD_STP_INPUT_STATUS(?,?)"};
                    //String[] storeCall = {"{call MD_STP_GEN_ALL_SUPP_EMP(?,?)}"};
                    
                    String[] storeParams1 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    String[] storeParams2 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    String[] storeParams3 = {userInfo.getUserName(),IMDSServices.INSTANCE.getClientIpAddress()};
                    List<String[]> paramList = new ArrayList<String[]>();
                    paramList.add(storeParams1);
                    paramList.add(storeParams2);
                    paramList.add(storeParams3);
                    ImportMultipleRunner runner = 
                               new ImportMultipleRunner(storeCall,paramList);
                         Thread importThread  = new Thread(runner);
                    
                    importThread.start();
                    log("ImportRunner name["+importThread.getName()+"]");
                } catch (IOException e) {
                    showError(e);
                }
               
                    
                fileName = fileVal.getFilename();
                log("File Content["+fileVal.getContentType()+"]");
            // Reset inputFile component after upload
                ResetUtils.reset(vce.getComponent());
            }
        }
        //showInfo("Upload Error With ["+fileName+"]");
        showInfo("Processing Mapping");
    }
    public final void onClickUploadExcel(ActionEvent event) {
        try {
            
            // showInfo("Upload Excel");
        } catch (Exception e) {
            showError(e);
        }
    }
    
    
    public final void onClickUpdateError(ActionEvent event) {
        try {
            log("Process Update Error");
        } catch (Exception e) {
            showError(e);
        }
    }
    
    public void onClickCancel(ActionEvent event) {
        ELog.log("=== On Click Cancel ==");
        if( popupEdit != null){
            popupEdit.cancel();    
        }        

        editMode = false;
        
        editRow = beforeEditRow;
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
        AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
    }
    private final boolean validateCriteria() {
        boolean isInvalid = false;
        if (config.criteria.hasBlockCriteria()) {

            for (String column : config.criteria.blockAtSign) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().equalsIgnoreCase("@")) {
                    showError("@ is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockComma) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().contains(",")) {
                    showError(", is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockPercent) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().contains("%")) {
                    showError("% is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockPower) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().equalsIgnoreCase("^")) {
                    showError("^ is not allow for " + column);
                    isInvalid = true;
                }
            }

        }

        return !isInvalid;
    }
    
    public final void onClickSearchData(ActionEvent event) {
        
        ELog.trace(programCode, userInfo, "onClickSearch - Start");
        
        try {

            /*** FRAMEWORK VALIDATE ***/

            if (validateCriteria() && validateBeforeSearch(criteria)) {

                ELog.log("screenSQL : " + screenSQL);

                screenSQL = getTableSQL();
                queryParam = new HashMap<String, Object>(criteria);   
                
                if(queryParam!=null&&!queryParam.isEmpty()){
                    if(queryParam.get("UPG_CODE")!=null){
                        String contentCode = queryParam.get("UPG_CODE").toString().toLowerCase();
                        queryParam.remove("UPG_CODE");
                        queryParam.put("LOWER(UPG_CODE)", contentCode);
                    }
                    
                    if(queryParam.get("EPL_PART_NO")!=null){
                        String contentName = queryParam.get("EPL_PART_NO").toString().toLowerCase();
                        queryParam.remove("EPL_PART_NO");
                        queryParam.put("LOWER(EPL_PART_NO)", contentName); 
                    }
                    if(queryParam.get("PARENT_PART")!=null){
                        String contentCode = queryParam.get("PARENT_PART").toString().toLowerCase();
                        queryParam.remove("PARENT_PART");
                        queryParam.put("LOWER(PARENT_PART)", contentCode);
                    }
                    
                    if(queryParam.get("ORDER_P_NO")!=null){
                        String contentName = queryParam.get("ORDER_P_NO").toString().toLowerCase();
                        queryParam.remove("ORDER_P_NO");
                        queryParam.put("LOWER(ORDER_P_NO)", contentName); 
                    }
                    if(queryParam.get("MAPPING_TYPE")!=null){
                        String contentCode = queryParam.get("MAPPING_TYPE").toString().toLowerCase();
                        queryParam.remove("MAPPING_TYPE");
                        queryParam.put("LOWER(MAPPING_TYPE)", contentCode);
                    }
                    
                    if(queryParam.get("SUPPLIER_CODE")!=null){
                        String contentName = queryParam.get("SUPPLIER_CODE").toString().toLowerCase();
                        queryParam.remove("SUPPLIER_CODE");
                        queryParam.put("LOWER(SUPPLIER_CODE)", contentName); 
                    }
                }

                beforeSearch(queryParam);
                
                rzTable.setFirst(0);
                startPagingIndex = 0;
                endPagingIndex = rzTable.getFetchSize();

                rzTable.setValue(tableData);
                tableData.clear();

                tableData.addAll(RzJDBCUtils.INSTANCE.queryForTable(programCode,userInfo,screenSQL, queryParam, 1, rzTable.getFetchSize(), config.criteria, resultData));

                afterSearch(resultData, event);
                checkVisible();
                AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                
            }
            ELog.trace(programCode, userInfo, "onClickSearch - finish");
        } catch (Exception e) {
            
            showError(e);
        }

    }
    public boolean checkResultAfterProcessUpdate(List<String> extraSQLList, int[] result) {
        return true;
    }
    
    public boolean checkResultBeforeProcessUpdate(List<String> extraSQLList, int[] result) {
        return true;
    }
    
    public void processBeforeUpdate(Map<String, Object> editRow, List<String> extraSQLList) {
    }
    
    public boolean validateBeforeUpdate(Map<String, Object> beforeEditRow, Map<String, Object> editRow) {
        return true;
    }
    
    public void beforeUpdate(Map<String, ?> beforeEditRow, Map<String, Object> editRow) {
    }  
    
    public void processAfterUpdate(Map<String, Object> before, Map<String, Object> after,List<String> extraSQLList) {}

    
    public boolean validateBeforeEdit(Map<String, Object> editRow) {
        return true;
    }
    
    public void beforeEdit(Map<String, Object> beforeEditRow, Map<String, Object> editRow) {
        
    }

}
