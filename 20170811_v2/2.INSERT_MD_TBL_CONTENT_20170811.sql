INSERT INTO MD_TBL_CONTENT (CONTENT_CODE, CONTENT_NAME, CONTENT_TYPE, MENU_ID, CONTENT_HEAD, CONTENT_DETAIL, CONTENT_FOOTER, RECORD_STATUS, CREATE_DATE, CREATE_BY, CREATE_IP, UPDATE_DATE, UPDATE_BY, UPDATE_IP) VALUES ('EM001', 'MMTh ''s Supplier ( inactive )', 'M', 'MD0001S', 'Subject:  Request MMTh''s Suppliers to input IMDS data (CR45 China model) ', 'First of all, we would like to express our deepest appreciation for your cooperation.

    Refer to MMTh''s official announcement about IMDS Activity for China Regulation (CNCA-020-023:2008) for getting recyclability Type Approval before exporting to China market.

    At this moment, we would like to request you input IMDS data for CR45 China models base on our parts list for all remained parts.x
    
    Please take action as following schedule.
 
================= Data Input Plan ====================
1.    Input Schedule
        -  Complete input data within 10 June''2011.

2.   Object Part Numbers
      Please see detail in attached sheet.
      Note; 
      - P/no.with " * " mark means it already get approval from MMC, supplier no need to take action
      - P/no.without" * "mark means supplier has not input IMDS data yet or not get approval from MMC 
         Except : Dummy parts : will not show Mark "*"

3.    ID Number for sending IMDS data to MMTh
       Please use the following Company name, Organization and Company ID.
       Company name		         Organization           Company ID
        Mitsubishi Motors Corporation         MMTh	          75984

If you have any questions or problems, please don''t hesitate to contact us anytime.

MMTh contact person ;

1.Mr.Pitukpong Suebsai, Tel: 038-498642.
   E-mail : pitukpong.suebsai@th.mitsubishi-motors.com
2.Mr.Chanchai Leejaroenporn, Tel.038-498673.
   E-mail ; chanchai.leejaroenporn@th.mitsubishi-motors.com

Thank you very much for your good cooperation.
Best Regards,', 'With Best Regards .', 'N', TO_DATE('2008-06-03 18:58:20', 'YYYY-MM-DD HH24:MI:SS'), 'Phatchaya.pak', '10.146.202.217', TO_DATE('2017-08-10 19:04:52', 'YYYY-MM-DD HH24:MI:SS'), 'TEETAT.NIT', '10.146.202.217');
INSERT INTO MD_TBL_CONTENT (CONTENT_CODE, CONTENT_NAME, CONTENT_TYPE, MENU_ID, CONTENT_HEAD, CONTENT_DETAIL, CONTENT_FOOTER, RECORD_STATUS, CREATE_DATE, CREATE_BY, CREATE_IP, UPDATE_DATE, UPDATE_BY, UPDATE_IP) VALUES ('EM003', 'Reject Report', 'M', 'MD0010M', 'Subject:  Request MMTh''s Suppliers to input IMDS data for CR27/47 models.(3E00)', 'Dear Sirs, 

   First of all, we would like to express our deepest appreciation for your good cooperation.

   At this moment, we would like to request supplier to continue input IMDS data for 

CR27/47 (3E00) as schedule as follows.
 
================= Data Input Plan ====================
1.    Schedule
       CR27/47(3E00) : Target input complete on 20 Jul. 2009.

        Note: Now already behigh target, Supplier must complete within 31 Jul''09 
                 If you can not complete as Target please make report to MMTh,( Cause of delay 1 Plan)

2.   Object Part Numbers.
       Please see detail in attached sheet
       Note ; ''*'' mark that show in report ; is common part with CR45 model that you already 
       input and get approval from MMC. So, no need to input again.

3.    ID Number for send IMDS data to MMTh
        Please use the following name, organization and ID.

       Company name		     Organization             Company ID
       Mitsubishi Motors Corporation     MMTh	          75984

If you have any questions and problems, please don''t hesitate to contact us anytime.
    
Best Regards,
MMTh ELV Team', null, 'N', TO_DATE('2009-02-04 08:36:56', 'YYYY-MM-DD HH24:MI:SS'), 'chanchai.lee', '10.146.202.217', TO_DATE('2009-08-17 15:50:34', 'YYYY-MM-DD HH24:MI:SS'), 'chanchai.lee', '10.146.202.217');
INSERT INTO MD_TBL_CONTENT (CONTENT_CODE, CONTENT_NAME, CONTENT_TYPE, MENU_ID, CONTENT_HEAD, CONTENT_DETAIL, CONTENT_FOOTER, RECORD_STATUS, CREATE_DATE, CREATE_BY, CREATE_IP, UPDATE_DATE, UPDATE_BY, UPDATE_IP) VALUES ('EM002', 'Send Request To MMC', 'M', 'MD0009M', 'IMDS Input request /Status list Report', 'test', 'Thank you very much for your bast support us every time.
we are appreciated 1 regards
MMTh ELV Team. ', 'Y', TO_DATE('2008-10-06 11:05:24', 'YYYY-MM-DD HH24:MI:SS'), 'chanchai.lee', '10.146.248.78', TO_DATE('2012-12-10 08:47:48', 'YYYY-MM-DD HH24:MI:SS'), 'Pitukpong.sue', '10.146.248.78');
INSERT INTO MD_TBL_CONTENT (CONTENT_CODE, CONTENT_NAME, CONTENT_TYPE, MENU_ID, CONTENT_HEAD, CONTENT_DETAIL, CONTENT_FOOTER, RECORD_STATUS, CREATE_DATE, CREATE_BY, CREATE_IP, UPDATE_DATE, UPDATE_BY, UPDATE_IP) VALUES ('EM002', 'Send Request To MMC (inactive)', 'M', 'MD0008M', 'Request to input and update IMDS data for Global Small (EL Model) 
', 'Dear all MMTh suppliers,

   First of all, we would like to express our deepest appreciation for your good cooperation to proceed  IMDS data input activity in last year.

   Anyway until now, new parts are released and also some part structure have been changed due to change of Engineering Order (EO). So, we would like to send part list to you again for the update of part structure and new parts request.

   We would like to request you to check part list request as the attached file, 
   If part structure change(add or delete child part), please update IMDS data again.
   If new part releasing ; please input IMDS data as request sheet.
  
   Please complete input within 09 Mar 2012

================= Data Input Plan ====================
1. Target of data complete.
    Supplier have to finish input data within 09 Mar. 2012.
                 
2. Objected Part Numbers. Please see detail in attached sheet. 
          
       Note ; ''*'' mark show on report ; IMDS data of such part already get approval from MMC.
                 No need to input again except child part that is under new parent part. Supplier must
                 input all structure.
               
3. Please update IMDS data base on Engineering Order (EO) and LCPO. 
  
4. ID Number for sending IMDS data to MMTh 
        
Please use the following name, organization and ID.

       Company name		     Organization             Company ID
       Mitsubishi Motors Corporation     MMTh	          75984 

If you have any questions or problems, please don''t hesitate to contact us.
         
Best Regards,
MMTh ELV Team', null, 'N', TO_DATE('2011-08-02 11:38:46', 'YYYY-MM-DD HH24:MI:SS'), 'chanchai.lee', '10.146.248.77', TO_DATE('2012-12-10 08:57:54', 'YYYY-MM-DD HH24:MI:SS'), 'Pitukpong.sue', '10.146.248.77');
INSERT INTO MD_TBL_CONTENT (CONTENT_CODE, CONTENT_NAME, CONTENT_TYPE, MENU_ID, CONTENT_HEAD, CONTENT_DETAIL, CONTENT_FOOTER, RECORD_STATUS, CREATE_DATE, CREATE_BY, CREATE_IP, UPDATE_DATE, UPDATE_BY, UPDATE_IP) VALUES ('EM001', 'Request MMTh''s Suppliers input IMDS data for 3V41(EL-L) model.', 'M', 'MD0101P', 'Request MMTh''s Suppliers input IMDS data for 3V41(EL-L) model.', 'First of all, we would like to express our deepest appreciation for your cooperation.

    Refer to MMTh''s official announcement about IMDS Activity for China Regulation (CNCA-020-023:2008) for getting recyclability Type Approval before exporting to China market.

    At this moment, we would like to request you input IMDS data for CR45 China models base on our parts list for all remained parts.x
    
    Please take action as following schedule.
 
================= Data Input Plan ====================
1.    Input Schedule
        -  Complete input data within 10 June''2011.

2.   Object Part Numbers
      Please see detail in attached sheet.
      Note; 
      - P/no.with " * " mark means it already get approval from MMC, supplier no need to take action
      - P/no.without" * "mark means supplier has not input IMDS data yet or not get approval from MMC 
         Except : Dummy parts : will not show Mark "*"

3.    ID Number for sending IMDS data to MMTh
       Please use the following Company name, Organization and Company ID.
       Company name		         Organization           Company ID
        Mitsubishi Motors Corporation         MMTh	          75984

If you have any questions or problems, please don''t hesitate to contact us anytime.

MMTh contact person ;

1.Mr.Pitukpong Suebsai, Tel: 038-498642.
   E-mail : pitukpong.suebsai@th.mitsubishi-motors.com
2.Mr.Chanchai Leejaroenporn, Tel.038-498673.
   E-mail ; chanchai.leejaroenporn@th.mitsubishi-motors.com

Thank you very much for your good cooperation.
Best Regards,', null, 'Y', TO_DATE('2012-12-10 08:18:09', 'YYYY-MM-DD HH24:MI:SS'), 'Pitukpong.sue', '10.146.248.77', TO_DATE('2017-08-10 19:04:52', 'YYYY-MM-DD HH24:MI:SS'), 'TEETAT.NIT', '10.146.248.77');