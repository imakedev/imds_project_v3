package com.mmth.imds.backing.mst;

import com.mmth.framework.util.ELog;
import com.mmth.framework.util.EStringUtils;
import com.mmth.framework.util.RzJDBCUtils;
import com.mmth.imds.model.master.SupplierModel;
import com.mmth.imds.model.report.RPMd0001r01TO;
import com.mmth.imds.service.IMDSServices;
import com.mmth.mdf.controller.config.RzInitialConfig;
import com.mmth.mdf.controller.template.MDFTMST02;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.myfaces.trinidad.event.RangeChangeEvent;
import org.apache.myfaces.trinidad.event.SortEvent;
import org.apache.myfaces.trinidad.model.RowKeySet;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class MD0002M01 extends MDFTMST02 {
    private boolean suppCanAdd;
    private boolean suppCanDelete;
    public MD0002M01() {
        super();
    }

    public void setSuppCanAdd(boolean suppCanAdd) {
        this.suppCanAdd = suppCanAdd;
    }

    public boolean isSuppCanAdd() {
        return suppCanAdd;
    }

    public void setSuppCanDelete(boolean suppCanDelete) {
        this.suppCanDelete = suppCanDelete;
    }

    public boolean isSuppCanDelete() {
        return suppCanDelete;
    }

    @Override
    public void preRender(ComponentSystemEvent componentSystemEvent) {
        // TODO Implement this method
        log("into preRenderx ");
        super.preRender(componentSystemEvent);
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> requestMap = context.getExternalContext().getRequestParameterMap();
        editMode = false;
        canAccess = canAdd = canEdit = canDelete = canProcess = canSearch = suppCanAdd = suppCanDelete = true;
        log("userInfo.isVendor()"+userInfo.isVendor());
        if (userInfo.isVendor()) {
            suppCanAdd = false;
            suppCanDelete = false;
        } 
        /* */
        //log("into requestMap1 "+requestMap.size());
        //log("into requestMap2 "+requestMap.get("status"));
        if (requestMap.get("status") != null) {
            //this.onClickClear();
            this.onClickSearchData();
            log("into preRenderx "+requestMap.get("status"));
        }
    }
    
    @Override
    public String getTableName() {
        // TODO Implement this method
        return "MD_TBL_SUPPLIER_MASTER";
    }

    @Override
    public String[] getPKColumn() {
        // TODO Implement this method
        return new String[]{"SUPPLIER_ID"};
    }

    @Override
    protected String getTableSQL() {
        // TODO Implement this method
//        return "SELECT * FROM MD_TBL_SUPPLIER_MASTER WHERE CREATE_DATE IS NOT NULL";
        return "SELECT sm.*,st.SUPPLIER_TYPE_NAME " +
               // for 12c
               /*
            ", ( "+
 " SELECT LISTAGG(sm2.MAIL_ADDRESS, ',') WITHIN GROUP (ORDER BY sm2.SUPPLIER_SEQ) "+
" FROM MD_TBL_SUPPLIER_MAIL sm2 where sm2.SUPPLIER_ID =  sm.SUPPLIER_ID "+
" and  sm2.MAIL_TYPE='T' "+
" GROUP BY sm2.SUPPLIER_ID ) as SUPPLIER_MAIL_TO, "+
" ( "+
  "  SELECT LISTAGG(sm2.MAIL_ADDRESS, ',') WITHIN GROUP (ORDER BY sm2.SUPPLIER_SEQ) "+
" FROM MD_TBL_SUPPLIER_MAIL sm2 where sm2.SUPPLIER_ID =  sm.SUPPLIER_ID "+
" and  sm2.MAIL_TYPE='C' "+
" GROUP BY sm2.SUPPLIER_ID ) as SUPPLIER_MAIL_CC "+
*/
               // end for 12 c
               
               // for 10g
               ", ( "+
               " SELECT  wm_concat(sm2.MAIL_ADDRESS) AS SUPPLIER_MAIL_TO "+
               " FROM MD_TBL_SUPPLIER_MAIL sm2 where sm2.SUPPLIER_ID =  sm.SUPPLIER_ID "+
               " and  sm2.MAIL_TYPE='T' "+
               " GROUP BY sm2.SUPPLIER_ID ) as SUPPLIER_MAIL_TO, "+
               "   (SELECT  wm_concat(sm2.MAIL_ADDRESS) AS SUPPLIER_MAIL_TO "+
               " FROM MD_TBL_SUPPLIER_MAIL sm2 where sm2.SUPPLIER_ID =  sm.SUPPLIER_ID "+
               " and  sm2.MAIL_TYPE='C' "+
               " GROUP BY sm2.SUPPLIER_ID ) as SUPPLIER_MAIL_CC "+
             // end for 10g                           
            "FROM MD_TBL_SUPPLIER_MASTER sm left join MD_TBL_SUPPLIER_TYPE st on st.SUPPLIER_TYPE_ID = sm.SUPPLIER_TYPE WHERE sm.CREATE_DATE IS NOT NULL ";
    }

    @Override
    public void init(RzInitialConfig rzInitialConfig) {
        // TODO Implement this method
        config.bypassAccessRight = true;
        config.export.excelColumnDB = new String[]{"SUPPLIER_ID","SUPPLIER_NAME_ENG","COMPANY_ID","CONTACT_NAME","CONTACT_PHONE_NO","CONTACT_NAME_MGR","MANAGER_PHONE_NO","SUPPLIER_TYPE_NAME","SUPPLIER_MAIL_TO","SUPPLIER_MAIL_CC"};
        config.export.excelHeader = config.export.excelColumnDB ;
        config.export.usePOI = true;
        getTableSQL();
        rzTable.setFetchSize(50);
        rzTable.setRows(50);
        this.onClickSearchData();
        log("into init ");
    }
   
    /*public final void onClickEdit() {
        try {
            if (this.getPKColumn().length > 0) {

                if ((Map<String, Object>) this.rzTable.getSelectedRowData() == null) {
                    throw new Exception("nullrecord");
                }

                editRow = (Map<String, Object>) this.rzTable.getSelectedRowData();

                log(editRow);
                if (validateBeforeEdit(editRow)) {
                    beforeEditRow = new LinkedHashMap<String, Object>(editRow);
                    beforeEdit(beforeEditRow, editRow);

                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    hints.add(RichPopup.PopupHints.HintTypes.HINT_ALIGN, RichPopup.PopupHints.AlignTypes.ALIGN_OVERLAP);
                    popupEdit.show(hints);

                }

            } else {
                showError("[PROGRAMMER BUG] Please set getPKColumn().");
            }
        } catch (Exception e) {
            ELog.log("error: " + e.toString());
            if (e.getMessage().equals("nullrecord")) {
                showError("Please select record.");
            } else {
                showError(e);
            }
        }
    }*/
    
    public void onClickAdd() {
     try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            info("uri: "+ec.getRequestContextPath());
            ec.redirect(ec.getRequestContextPath() + "/faces/imds/master/MD0002M02.jsf?status="+"c");
        } catch (Exception e) {
            //showError("Please select only one record########."); 
            e.printStackTrace();
        }
    }
    
    public void onClickEdit() {
     try {
        RowKeySet editRowList = this.rzTable.getSelectedRowKeys();
        editList.clear();
        String id = null;
        info("selectRowSet : "+editRowList.toString());
        info("selectRowSet size : "+editRowList.size());

        Map<String, Object>  record;
        if(editRowList != null && editRowList.size() == 1){
            
            for(Object key : editRowList.toArray() ){
                
                record = (Map<String, Object> )rzTable.getRowData(key);
                record.put("#BEFORE", new HashMap<String,Object>(record));
                record.put("#EDIT",true);
                editList.add(record);
            }
            
            
            info("selectRowSet : "+editList.toString());
            
            for(Map<String, Object> map:editList){
                id = map.get("SUPPLIER_ID").toString();
            }
            
            editMode = true;
           
        }else if(editRowList != null && editRowList.size()>1){           
            showError("Please select only one record.");           
        }else {
            showError("Please select record.");
            throw new FacesException("Redirection failed");
        }
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            info("uri: "+ec.getRequestContextPath());
            ec.redirect(ec.getRequestContextPath() + "/faces/imds/master/MD0002M03.jsf?id=" + id);
        } catch (Exception e) {
            //showError("Please select only one record########."); 
            e.printStackTrace();
        }
    }
    
    public final void onClickDeleteData(ActionEvent event) {

        ELog.log("=== On Click Delete ==");

        Map<String, Object> selectRow = (Map<String, Object>) this.rzTable.getSelectedRowData();

        if (selectRow == null) {
            showError("Please select record.");
        } else {


            if (validateBeforeDelete(selectRow)) {
                try {
                    beforeDelete(selectRow);

                    if (processDelete(selectRow)&&processDeleteSuppEmail(selectRow)) {
                       
                            afterDelete(selectRow);
                            
                            if (config.searchAfterDelete) {
                                this.onClickSearchData();
                            }
                       
                    } else {
                        //showError("Not Any Delete records ?");
                    }
                    
                    this.onClickSearchData();
                    showInfo("Delete Successfully.");

                } catch (Exception e) {
                    showError(e);
                }
            }
        }
    }
    
    
    public boolean processDeleteSuppEmail(Map<String, Object> record) throws SQLException {

        if (record.keySet().size() > 0) {
            
            StringBuilder deleteSQL = new StringBuilder("DELETE FROM MD_TBL_SUPPLIER_MAIL WHERE ");
            List<Object> PKParam = new LinkedList<Object>();

            for (String pk : getPKColumn()) {
                deleteSQL.append(" ").append(pk).append(" = ? AND");
                PKParam.add(record.get(pk));
            }
            
            if (config.master.processAfterDelete) {
                SingleConnectionDataSource sds = new SingleConnectionDataSource(RzJDBCUtils.INSTANCE.getJDBCTemplate().getDataSource().getConnection(), true);
                JdbcTemplate template = new JdbcTemplate(sds);
                
                try {
                    
                    template.getDataSource().getConnection().setAutoCommit(false);
                    
                    int rowEffect = RzJDBCUtils.INSTANCE.update(EStringUtils.INSTANCE.substringBeforeLast(deleteSQL.toString(), "AND"), PKParam);

                    List<String> extraSQLList = new ArrayList<String>();
                    processAfterDelete(record, extraSQLList);

                    if (extraSQLList.size() > 0) {
                        int[] result = template.batchUpdate(extraSQLList.toArray(new String[0]));

                        if (checkResultAfterProcessDelete(extraSQLList, result)) {
                            template.getDataSource().getConnection().commit();
                        } else {
                            template.getDataSource().getConnection().rollback();
                        }
                    }
                    
                    ELog.log("Row Effect : " + rowEffect);
                    return (rowEffect != 0);

                } catch (Exception e) {
                    showError(e);
                } finally {
                    template.getDataSource().getConnection().close();
                    sds.destroy();
                }


            } else {
                int rowEffect = RzJDBCUtils.INSTANCE.update(EStringUtils.INSTANCE.substringBeforeLast(deleteSQL.toString(), "AND"), PKParam);
                ELog.log("Row Effect : " + rowEffect);

                return (rowEffect != 0);
            }
                
        } else {
            showError("[PROGRAMER BUG] Not Any Assign Create Field.");
        }

        return true;
    }
    
    
    public void exportPDF(FacesContext fc, OutputStream out){
                
        StringBuffer criteriaSQL = new StringBuffer();
        queryParam = new HashMap<String, Object>(criteria);
        
        String supCode = queryParam.get("sm.SUPPLIER_ID")!=null?(String)queryParam.get("sm.SUPPLIER_ID"):"";
        String supName = queryParam.get("sm.SUPPLIER_NAME_ENG")!=null?(String)queryParam.get("sm.SUPPLIER_NAME_ENG"):"";
        
        if(supCode.length()>0){
            if(supCode.indexOf("%")!=-1){
                criteriaSQL.append(" AND UPPER(sm.SUPPLIER_ID) like '"+supCode.toUpperCase()+"' "); 
            }else{
                criteriaSQL.append(" AND UPPER(sm.SUPPLIER_ID) = '"+supCode.toUpperCase()+"' "); 
            } 
        }
        if(supName.length()>0){
                if(supCode.indexOf("%")!=-1){
                    criteriaSQL.append(" AND UPPER(sm.SUPPLIER_NAME_ENG) like '"+supName.toUpperCase()+"' ");
                }else{
                    criteriaSQL.append(" AND UPPER(sm.SUPPLIER_NAME_ENG) = '"+supName.toUpperCase()+"' ");
                }
            }
        if (userInfo.isVendor()) {
            criteriaSQL.append(" AND UPPER(sm.SUPPLIER_ID) = '"+userInfo.getVendorCode().toUpperCase()+"' "); 
        } 
        
        String sqlData = "SELECT * FROM MD_TBL_SUPPLIER_MASTER sm left join MD_TBL_SUPPLIER_TYPE st on st.SUPPLIER_TYPE_ID = sm.SUPPLIER_TYPE\n" + 
        " WHERE 1=1 "+criteriaSQL.toString()+" ORDER BY sm.SUPPLIER_ID  ";
       // RzJDBCUtils.INSTANCE.queryForTable(programCode,userInfo,screenSQL, queryParam, 1, rzTable.getFetchSize(), config.criteria, resultData)
        log("sqlData="+sqlData);
        List list = RzJDBCUtils.INSTANCE.getJDBCTemplate().queryForList(sqlData);
            List<SupplierModel> collections = new ArrayList<SupplierModel>();       
            if(list!=null && list.size()>0){
                for(int i=0;i<list.size();i++){
                    Map map = (Map)list.get(i);
                    SupplierModel sup = new SupplierModel();
                    sup.setSupplierCode(map.get("SUPPLIER_ID").toString());
                    sup.setSupplierName(map.get("SUPPLIER_NAME_ENG")!=null?map.get("SUPPLIER_NAME_ENG").toString():"");
                    sup.setCompanyId(map.get("COMPANY_ID")!=null?map.get("COMPANY_ID").toString():"");
                    sup.setImdsUserName(map.get("CONTACT_NAME")!=null?map.get("CONTACT_NAME").toString():"");
                    String imdsPhoneStr = "";
                    imdsPhoneStr = map.get("CONTACT_PHONE_NO")!=null?map.get("CONTACT_PHONE_NO").toString():"";
                    String imdsPhoneStrBlackSpece = imdsPhoneStr.replace("/", " / ").trim();
                    String imdsPhoneStrComma = imdsPhoneStr.replace(",", " , ").trim();
                    sup.setImdsPhoneNo(imdsPhoneStrComma);
                    sup.setClientName(map.get("CONTACT_NAME_MGR")!=null?map.get("CONTACT_NAME_MGR").toString():"");
                    String clientPhoneStr = "";
                    clientPhoneStr = map.get("MANAGER_PHONE_NO")!=null?map.get("MANAGER_PHONE_NO").toString():"";
                    String clientPhoneStrBlackSpece = clientPhoneStr.replace("/", " / ").trim();
                    String clientPhoneStrComma = clientPhoneStrBlackSpece.replace(",", " , ").trim();
                    sup.setClientPhoneNo(clientPhoneStrComma);
                    sup.setSupplierType(map.get("SUPPLIER_TYPE_NAME")!=null?map.get("SUPPLIER_TYPE_NAME").toString():"");
                    
                    collections.add(sup);
                }
            }   
        SupplierModel documentObject = new SupplierModel();
            if(collections.size()>0){
                documentObject = collections.get(0);
            }
        String jasperFileName = "MD0002M01";
        //IMDSServices.INSTANCE.exportPDF(fc,out,jasperFileName,collections,documentObject);
        IMDSServices.INSTANCE.exportPDF(jasperFileName,collections,documentObject);
        ELog.trace(programCode, userInfo, "onClickExportExcel - Start");
        if (userInfo.isVendor()) {
            suppCanAdd = false;
            suppCanDelete = false;
        } 
    }
    
    
    private SupplerMasterListBean jaxbXMLToObject( String fileNameXml ) {
        try {
            JAXBContext context = JAXBContext.newInstance(SupplerMasterListBean.class);
            Unmarshaller un = context.createUnmarshaller();
            SupplerMasterListBean emp = (SupplerMasterListBean) un.unmarshal(new File(fileNameXml));
            return emp;
         } catch (JAXBException e) {
            e.printStackTrace();
         }
        return null;
    }
    
    
    private void jaxbObjectToXML(SupplerMasterListBean emp,String fileNameXml) {
        try {
                JAXBContext context = JAXBContext.newInstance(SupplerMasterListBean.class);
                Marshaller m = context.createMarshaller();
                //for pretty-print XML in JAXB
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                
                // Write to File
                m.marshal(emp, new File(fileNameXml));
        } catch (JAXBException e) {
                e.printStackTrace();
        }
    }
    
    
    
    public  void convertToPDF(FacesContext fc, OutputStream out)  throws IOException, FOPException, TransformerException {
            
            ServletContext con = (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
            String fileName  = config.export.fileName + ".pdf";
            
            // the XSL FO file
            File xsltFile = new File(con.getRealPath("templatepdf")+"\\templates.xsl");
            // the XML file which provides the input
            StreamSource xmlSource = new StreamSource(new File(con.getRealPath("templatepdf")+"\\supplier.xml"));
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
          
            //Prepare response                
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();    
               
            try {
                
                ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
                
                // Construct fop with desired output format
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, bOutput);

                // Setup XSLT
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

                // Resulting SAX events (the generated FO) must be piped through to FOP
                Result res = new SAXResult(fop.getDefaultHandler());

                // Start XSLT transformation and FOP processing
                // That's where the XML is first transformed to XSL-FO and then 
                // PDF is created
                transformer.transform(xmlSource, res);
                       
                //Prepare response
                response.setContentType("application/pdf");
                response.setHeader("Content-Disposition","attachment; filename="+fileName);
                response.setContentLength(bOutput.size());

                //Send content to Browser
                response.getOutputStream().write(bOutput.toByteArray());
                response.getOutputStream().flush();
            } finally {
                out.close();
            }
        }
                  
                    
    public final void onClickSearchData(ActionEvent event) {
        
        ELog.trace(programCode, userInfo, "onClickSearch - Start event");
        
        try {

            /*** FRAMEWORK VALIDATE ***/
                        
            if (validateCriteria() && validateBeforeSearch(criteria)) {

                ELog.log("screenSQL : " + screenSQL);

                screenSQL = getTableSQL();
                queryParam = new HashMap<String, Object>(criteria);   
                
                if(queryParam!=null&&!queryParam.isEmpty()){
                    
                    if(queryParam.get("sm.SUPPLIER_ID")!=null){
                        String supplierId = queryParam.get("sm.SUPPLIER_ID").toString().toLowerCase();
                        queryParam.remove("sm.SUPPLIER_ID");
                        queryParam.put("LOWER(sm.SUPPLIER_ID)", supplierId);
                        //screenSQL = screenSQL+" AND LOWER(SUPPLIER_ID) = '"+supplierId+"'";
                    }
                    
                    if(queryParam.get("sm.SUPPLIER_NAME_ENG")!=null){
                        String supplierNameEng = queryParam.get("sm.SUPPLIER_NAME_ENG").toString().toLowerCase();
                        queryParam.remove("sm.SUPPLIER_NAME_ENG");
                        queryParam.put("LOWER(sm.SUPPLIER_NAME_ENG)", supplierNameEng); 
                        //screenSQL = screenSQL+" AND LOWER(SUPPLIER_NAME_ENG) = '"+supplierNameEng+"'";
                    }
                }
                if (userInfo.isVendor()) {
                    queryParam.put("UPPER(sm.SUPPLIER_ID)", userInfo.getVendorCode().toUpperCase());    
                } 
                screenSQL = screenSQL+" ORDER BY  sm.SUPPLIER_ID ASC , sm.CREATE_DATE DESC ";

                beforeSearch(queryParam);
                
                rzTable.setFirst(0);
                startPagingIndex = 0;
                endPagingIndex = rzTable.getFetchSize();

                rzTable.setValue(tableData);
                tableData.clear();
                log("my query action ="+screenSQL+" , queryParam="+queryParam.get("sm.SUPPLIER_ID"));
                tableData.addAll(RzJDBCUtils.INSTANCE.queryForTable(programCode,userInfo,screenSQL, queryParam, 1, rzTable.getFetchSize(), config.criteria, resultData));
                if (userInfo.isVendor()) {
                    suppCanAdd = false;
                    suppCanDelete = false;
                } 
                afterSearch(resultData, event);
                AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                
            }
            ELog.trace(programCode, userInfo, "onClickSearch - finish");
        } catch (Exception e) {
            
            showError(e);
        }

    }
    
    
    private final boolean validateCriteria() {
        boolean isInvalid = false;
        if (config.criteria.hasBlockCriteria()) {

            for (String column : config.criteria.blockAtSign) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().equalsIgnoreCase("@")) {
                    showError("@ is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockComma) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().contains(",")) {
                    showError(", is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockPercent) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().contains("%")) {
                    showError("% is not allow for " + column);
                    isInvalid = true;
                }
            }

            for (String column : config.criteria.blockPower) {
                if (EStringUtils.INSTANCE.isNotBlank(criteria.get(column)) && criteria.get(column).toString().equalsIgnoreCase("^")) {
                    showError("^ is not allow for " + column);
                    isInvalid = true;
                }
            }

        }

        return !isInvalid;
    }
    
    
    public void onClickClear() {
        try {
            ELog.trace(programCode, userInfo, "onClickClear - Start");
            criteria.clear();
            queryParam.clear();
            resultData.clear();
            tableData.clear();
            
            mustSortColumn = false;
            AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
            AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
            init(config);
            initForFramework(config);
            ELog.trace(programCode, userInfo, "onClickClear - finish");
        } catch (Exception e) {
            showError(e);
        }
    }
    
    
    public final void onClickSearchData() {
        
        ELog.trace(programCode, userInfo, "onClickSearch - Start normal");
        
        try {

            /*** FRAMEWORK VALIDATE ***/
            
            if (validateCriteria() && validateBeforeSearch(criteria)) {

                ELog.log("screenSQL : " + screenSQL);

                screenSQL = getTableSQL();
                queryParam = new HashMap<String, Object>(criteria);   
                
                if(queryParam!=null&&!queryParam.isEmpty()){
                    
                    if(queryParam.get("sm.SUPPLIER_ID")!=null){
                        String supplierId = queryParam.get("sm.SUPPLIER_ID").toString().toLowerCase();
                        queryParam.remove("sm.SUPPLIER_ID");
                        queryParam.put("LOWER(sm.SUPPLIER_ID)", supplierId);
                        //screenSQL = screenSQL+" AND LOWER(sm.SUPPLIER_ID) = '"+supplierId+"'";
                    }
                    
                    if(queryParam.get("sm.SUPPLIER_NAME_ENG")!=null){
                        String supplierNameEng = queryParam.get("sm.SUPPLIER_NAME_ENG").toString().toLowerCase();
                        queryParam.remove("sm.SUPPLIER_NAME_ENG");
                        queryParam.put("LOWER(sm.SUPPLIER_NAME_ENG)", supplierNameEng); 
                    }
                }
                //userInfo.setVendorCode("R015");
                if (userInfo.isVendor()) {
                    queryParam.put("UPPER(sm.SUPPLIER_ID)", userInfo.getVendorCode().toUpperCase());    
                } 
                screenSQL = screenSQL+" ORDER BY sm.SUPPLIER_ID ASC , sm.CREATE_DATE DESC ";

                beforeSearch(queryParam);
                
                rzTable.setFirst(0);
                startPagingIndex = 0;
                endPagingIndex = rzTable.getFetchSize();
                ELog.log("rzTable: "+rzTable.getFetchSize());

                rzTable.setValue(tableData);
                tableData.clear();
                log("my query="+screenSQL+" , queryParam="+queryParam.get("sm.SUPPLIER_ID"));
                tableData.addAll(RzJDBCUtils.INSTANCE.queryForTable(programCode,userInfo,screenSQL, queryParam, 1, rzTable.getFetchSize(), config.criteria, resultData));

                afterSearch(resultData);
                if (userInfo.isVendor()) {
                    suppCanAdd = false;
                    suppCanDelete = false;
                } 
                AdfFacesContext.getCurrentInstance().addPartialTarget(rzTable);
                AdfFacesContext.getCurrentInstance().addPartialTarget(getToolbar());
                AdfFacesContext.getCurrentInstance().addPartialTarget(getTotalBar());
                
            }
            ELog.trace(programCode, userInfo, "onClickSearch - finish");
           
        } catch (Exception e) {
            
            showError(e);
        }

    }
}
