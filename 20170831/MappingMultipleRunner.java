package com.mmth.imds.threads;

import com.mmth.framework.util.RzJDBCUtils;

import java.sql.CallableStatement;
import java.sql.SQLException;

public class MappingMultipleRunner implements Runnable {
    
    private String[] sqlCall;
    private String[] params;

    public MappingMultipleRunner(String[] sqlCall ,String[] params ) {
        this.sqlCall = sqlCall;
        this.params = params;;
        
    }

    public void run() {
        //...
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int k=0;k<this.sqlCall.length;k++){
            java.sql.Connection connection = null;
            CallableStatement callableStatement = null;
            int result  = 0;
            try {
                connection = RzJDBCUtils.INSTANCE.getConnection();
                callableStatement = connection.prepareCall(this.sqlCall[k]);
                String []paramsList = this.params;
                int paramSize = paramsList.length;
                for(int i=0;i<paramSize;i++){
                        callableStatement.setString(i+1,paramsList[i] );
                }
               result =  callableStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
                if(callableStatement!=null)
                    try {
                        callableStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                if(connection!=null){
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        String  updateStatusSql = " update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=2 where PROCESS_ID='MAPPING' ";
            
            
        RzJDBCUtils.INSTANCE.getJDBCTemplate().execute(updateStatusSql); 
       // System.out.println(" Runner "+this.sqlCall+" with result["+result+"]"+(new Date()));
    }
}
