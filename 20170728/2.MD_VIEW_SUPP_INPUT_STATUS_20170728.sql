CREATE or REPLACE VIEW MD_VIEW_SUPP_INPUT_STATUS AS SELECT
       input_status.SUPPLIER_CODE,
       input_status.SUM_OF_INPUT_RECORD,
       to_char(''||input_status.PERCENT_OF_INPUT_RECORD||'%') as PERCENT_OF_INPUT_RECORD,
       input_status.PROCESS_DATE,
       input_status.CREATE_DATE,
       input_status.CREATE_BY,
       input_status.CREATE_IP,
       input_status.UPDATE_DATE,
       input_status.UPDATE_BY,
       input_status.UPDATE_IP,
       input_status.TOTAL_RECORD,
       to_char(input_status.PERCENT_REMAIN||'%') as PERCENT_REMAIN,
       SUPP.SUPPLIER_NAME_ENG,
       SUPP.CONTACT_NAME as SUPPLIER_IN_CHARGE,
      -- SUPP.TEAM_LEADER as MMTH_IN_CHARGE,
       (select TEAM_LEADER from MD_TBL_USER_GROUP user_group
 where exists(
   SELECT * from MD_TBL_USER_SUPPLIER user_supp WHERE
   user_group.USER_GROUP=user_supp.USER_GROUP
   and user_supp.SUPPLIER_ID= input_status.SUPPLIER_CODE
 ) AND rownum = 1 GROUP BY TEAM_LEADER) AS MMTH_IN_CHARGE ,
       input_status.INCLD_DUMMY
  FROM MD_TBL_PROC_INPUT_STATUS_V2 input_status LEFT JOIN
    MD_TBL_SUPPLIER_MASTER SUPP on (input_status.SUPPLIER_CODE=SUPP.SUPPLIER_ID);


