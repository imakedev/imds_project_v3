create or replace PROCEDURE "MD_STP_GEN_BY_SYS" -- OK
(
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_PARENT_PART  VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  var1 VARCHAR2(500);
  v_process_status_d MD_TBL_CONTROL_MAPPING.PROCESS_STATUS_ID%TYPE;
  CURSOR C1 IS SELECT PARENT_PART ,MC_CODE-- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9  -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
        where 
       -- SUPPLIER_ID='Y067' and 
       -- PARENT_PART=P_PARENT_PART--'1005C643'
       -- PARENT_PART = EPL_PART_NO and 
        LEVEL_NO=1 and SUPPLIER_CODE='-' 
        -- and PARENT_PART = '3800A139'
        -- and MC_CODE=41
     -- PARENT_PART='1005C643' -- '1360A097'--'1584A906'
      -- where SUPPLIER_ID='S104' and PART_NO='1350A807V'
   --where SUPPLIER_ID='A035' and PART_NO='MN124448'
  -- where SUPPLIER_ID='S005' -- T664
   GROUP BY PARENT_PART,MC_CODE
  ;
  
  CURSOR C2 (C2_PARENT_PART IN varchar2,C2_MC_CODE IN varchar2)
  IS
     SELECT model.*,ROWID -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9 model -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
       where 
       -- SUPPLIER_ID='Y067' and 
      -- PARENT_PART='1584A906'
      model.PARENT_PART=C2_PARENT_PART
      and model.MC_CODE =C2_MC_CODE-- 54
      order by MC_CODE ,to_number(LEVEL_SEQ);--ORDER_ID;
    
  W_C1 C1%ROWTYPE;
  W_C2 C2%ROWTYPE;
  W_INDEX NUMBER;
  W_COUNT NUMBER;
  W_COUNT_DUMMY NUMBER;
  W_COUNT_DUMMY_STR VARCHAR2(255);
  W_PART_NO VARCHAR2(255);
  W_FORMAT VARCHAR2(255);
  W_DUMMY_PART_NO VARCHAR2(255);
  W_TMP_SUPPLIER_CODE VARCHAR2(255);
  W_PREV_SUPPLIER_CODE VARCHAR2(255);
  W_TMP_LEVEL_NO NUMBER;
BEGIN
-- update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=1 where PROCESS_ID='MAPPING';
  -- clear table for test
-- delete from MD_TBL_PART_REPORT9 r1;
W_FORMAT :='FM00';
commit;

  -- W_COUNT_ALL :=0;
 -- select SYSDATE into NOW_TIME from dual;
   OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
        BEGIN
          W_INDEX:=1;
           W_COUNT  := 0;
           W_COUNT_DUMMY :=0;
            W_TMP_SUPPLIER_CODE :='-';
            W_TMP_LEVEL_NO :=1;
          -- BEGIN
              OPEN C2(W_C1.PARENT_PART,W_C1.MC_CODE);
                FETCH C2 INTO W_C2;
                   WHILE NOT(C2%NOTFOUND)
                   
                   LOOP
                    
                   IF (W_C2.LEVEL_NO > 2 and W_C2.SUPPLIER_CODE = '-' ) THEN
                          W_TMP_SUPPLIER_CODE :='-';
                           FOR actual_item IN (
                             SELECT aa.SUPPLIER_CODE from (
                              select r.SUPPLIER_CODE from MD_TBL_PART_REPORT9 r 
                                 where r.PARENT_PART=W_C2.PARENT_PART
                                 and r.MC_CODE=W_C2.MC_CODE and 
                                  r.LEVEL_NO=(W_C2.LEVEL_NO-1) and 
                                  to_number(r.LEVEL_SEQ)<W_C2.LEVEL_SEQ order BY to_number(r.LEVEL_SEQ) desc  ) aa
                                  where ROWNUM = 1
                              )
                          LOOP
                              W_TMP_SUPPLIER_CODE :=actual_item.SUPPLIER_CODE;
                          END LOOP;
                          /* */
                          UPDATE MD_TBL_PART_REPORT9
                            SET SUPPLIER_CODE=W_TMP_SUPPLIER_CODE
                            WHERE ROWID=W_C2.ROWID ;
                          commit;
                         /* */
                   END IF;
                   /*
                   IF(W_C2.LEVEL_NO = 2 and W_C2.SUPPLIER_CODE != '-' ) THEN
                      W_PREV_SUPPLIER_CODE:=W_C2.SUPPLIER_CODE;
                    END IF;
                   IF W_C2.LEVEL_NO > 2 AND ( W_C2.LEVEL_NO-W_TMP_LEVEL_NO)>1 THEN
                      dbms_output.put_line('***************************');
                     
                      W_TMP_LEVEL_NO := W_TMP_LEVEL_NO+1;
                      if W_C2.SUPPLIER_CODE != '-' THEN
                        W_TMP_SUPPLIER_CODE :=W_PREV_SUPPLIER_CODE;--W_C2.SUPPLIER_CODE;
                      END if;
                   END IF;
                   */
                   /* 
                   IF W_C2.SUPPLIER_CODE ='-' THEN 
                     UPDATE MD_TBL_PART_REPORT9
                        SET SUPPLIER_CODE=W_TMP_SUPPLIER_CODE
                        WHERE ROWID=W_C2.ROWID ;
                        commit;
                    END IF;
                   */
                   /*
                    dbms_output.put_line(W_INDEX||' SUPPILER_CODE = '||W_C2.SUPPLIER_CODE||' PART_NO = '||W_C2.PART_NO
                    ||' W_C2.LEVEL_NO = '||W_C2.LEVEL_NO||' LEVEL_SEQ = '||W_C2.LEVEL_SEQ
                    ||' ( W_C2.LEVEL_NO-W_TMP_LEVEL_NO) = '||( W_C2.LEVEL_NO-W_TMP_LEVEL_NO)||' W_TMP_LEVEL_NO = '||W_TMP_LEVEL_NO
                    ||' W_TMP_SUPPLIER_CODE = '||W_TMP_SUPPLIER_CODE
                    ||' W_PREV_SUPPLIER_CODE = '||W_PREV_SUPPLIER_CODE);
                    */
                    /* */
                    /* */
                   
                    /* 
                    IF(W_C2.LEVEL_NO > 1 and W_C2.SUPPLIER_CODE != '-' ) THEN
                      W_PREV_SUPPLIER_CODE:=W_C2.SUPPLIER_CODE;
                    END IF;
                    */
                    W_INDEX :=W_INDEX+1;
                  FETCH C2 INTO W_C2;
                END LOOP;
            CLOSE C2;
       END; 
            
      FETCH C1 INTO W_C1;
   END LOOP;
   CLOSE C1;
     
   /* */ 
END;