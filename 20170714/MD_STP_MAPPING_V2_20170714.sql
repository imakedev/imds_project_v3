create or replace PROCEDURE "MD_STP_MAPPING_V2" -- OK

 (
  P_CRATED_BY      VARCHAR2,
  P_CRATED_IP      VARCHAR2
  -- P_USER_ID         VARCHAR2,
  -- P_COMPUTER_NAME   VARCHAR2
 )
 IS
  v_timestamp TIMESTAMP;
  v_process_status_d MD_TBL_CONTROL_MAPPING.PROCESS_STATUS_ID%TYPE;
  CURSOR C1 IS SELECT model.*,ROWID -- SUPPLIER_ID,PART_NO
  FROM MD_TBL_PART_REPORT9 model -- MD_TBL_PART_MASTER2 
      -- where SUPPLIER_ID='S005' and PART_NO='5900A693'
      --  where SUPPLIER_ID='P018' and PART_NO='1015B887'
    -- where SUPPLIER_ID='T025' and PART_NO='1003A294'
       -- where 
       -- SUPPLIER_ID='Y067' and 
     -- PARENT_PART='1005C643' -- '1350A809'-- '1005C643' -- '1360A097' -- '1584A906'
       -- PARENT_PART='3800A139'
      -- where SUPPLIER_ID='S104' and PART_NO='1350A807V'
   --where SUPPLIER_ID='A035' and PART_NO='MN124448'
  -- where SUPPLIER_ID='S005' -- T664
  -- GROUP BY SUPPLIER_ID,PART_NO
  ;
    
    CURSOR C_CLEAR_SUPP 
  IS
     SELECT cl.*,ROWID FROM  MD_TBL_PART_REPORT9 cl where  SUPPLIER_CODE ='-';-- and PARENT_PART='3800A139';-- 
     
    
  W_C1 C1%ROWTYPE;
  W_C_CLEAR_SUPP C_CLEAR_SUPP%ROWTYPE;
   
   W_COUNT_ALL NUMBER;
   W_COUNT NUMBER;
   W_COUNT_DUMMY NUMBER;
   W_IS_COUNT_DUMMY NUMBER;
   W_COUNT_DUMMY_STR VARCHAR2(255);
   W_PART_NO VARCHAR2(255);
   W_MAPPING_TYPE VARCHAR2(255);
  NOW_TIME DATE;
  W_SUPPLIER_CODE VARCHAR2(25);
  W_ORDER_P_NO VARCHAR2(255);
   NOW_SEQ VARCHAR2(255);
   var1 VARCHAR2(2000);
   W_MMC_APPR_COUNT NUMBER;
   W_MMC_APPR VARCHAR2(25);
BEGIN
update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=1 where PROCESS_ID='MAPPING';
  -- clear table for test
 delete from MD_TBL_PART_REPORT9 r1; 
 
W_SUPPLIER_CODE :='';
commit;

 
/*

EPL_PART_NO = INPUT_IMDS_P_NO = origin mmc
LOCAL_P_NO = ORDER_P_NO = find supp
ENG_DATA_P_NO = PARTNAME
*/

 -- var1 :='
 insert into MD_TBL_PART_REPORT9 (
                    SELECT
                     
	GL_NO ,
	MC_CODE ,
	UPG_CODE ,
	SERIES_NO ,
	PLVC ,
	PART_NAME ,
	PART_COMPOSITION ,
	PARENT_PART ,
	PART_NO ,
	DRAW_NO ,
	LEVEL_NO ,
	UPC_CODE ,
	SAFE_ID ,
	EO_NO ,
	CL ,
	DRAWING_MASS ,
	MASS_ID ,
	MATERIAL_CODE ,
	DRAWING_ID ,
	DRAWING_SPEC ,
	COLOR_ID ,
	CLS ,
	QUALITY_ID ,
	F_CLASSIFICATION ,
	to_number(QUANTITY) ,
	QUANTITY_ID ,
	PART_ID ,
	INSTALL_NO ,
	INSTALL_SYMBOL ,
	SEQ_NO ,
	ENTRY_GL_NO ,
	ENTRY_MC_CODE ,
	KD_SITUATION_ID ,
	COLID_ERR_ID ,
	DAIYOUZU_ID ,
	AR_ID ,
	null , -- CREATE_DATE DATE,
	null , -- CREATE_BY VARCHAR2(30 byte),
	null , -- CREATE_IP VARCHAR2(30 byte),
	null , -- UPDATE_DATE DATE,
	null , -- UPDATE_BY VARCHAR2(30 byte),
	null , -- UPDATE_IP VARCHAR2(30 byte)'||
  -- NOW_SEQ||' -- 
   '-', --SUPPLIER_CODE VARCHAR2(6 byte),
											TO_CHAR(5), -- MAPPING_TYPE VARCHAR2(20 byte),
											PART_NO, --  EPL_PART_NO VARCHAR2(255 byte),
											PART_NO, -- LOCAL_P_NO VARCHAR2(255 byte),
											PART_NO, -- ORDER_P_NO VARCHAR2(255 byte),
											 null, -- MMC_APPR VARCHAR2(255 byte) ,
                     
											PART_NAME, -- ENG_DATA_P_NO VARCHAR2(255 byte),
											PART_NO, -- INPUT_IMDS_P_NO VARCHAR2(255 byte) ,
											to_char(to_number(SEQ_NO)), -- LEVEL_SEQ VARCHAR2(255 byte),
   --null
   MD_SEQ_REPORT_MAPPING.nextval 
                    from MD_TBL_PART_MODEL2 model 
                   -- where model.KD_SITUATION_ID ='*'
                    -- and model.PART_NO=W_PART_ORDER_NO and ROWNUM = 1
                  );
                  -- dbms_output.put_line('var1 = '||var1);
                  -- EXECUTE IMMEDIATE var1;  
                  
                  commit;
 
  W_COUNT_ALL :=0;
  select SYSDATE into NOW_TIME from dual;
   OPEN C1;
   FETCH C1 INTO W_C1;
   WHILE NOT(C1%NOTFOUND)
   LOOP
       BEGIN 
            W_COUNT  := 0;
          W_COUNT_DUMMY :=0;
          W_MAPPING_TYPE :='0';
                  commit;
              SELECT count(1) INTO W_COUNT
              FROM MD_TBL_PART_MASTER2 mmth_part
              where mmth_part.PART_NO=W_C1.PARENT_PART  
              ; 
         --  dbms_output.put_line('ROWID = '||W_C1.ROWID||' W_COUNT='||W_COUNT);
            
            IF W_COUNT > 0 THEN
                W_MAPPING_TYPE :='1';
                FOR actual_item IN (
                           select *  FROM MD_TBL_PART_MASTER2 mmth_part
                                  where mmth_part.PART_NO=W_C1.PARENT_PART -- W_C1.PART_NO 
                                  and ROWNUM =1
                            )
                 LOOP
                             -- dbms_output.put_line('ROWID = '||W_C1.ROWID||' actual_item=' || actual_item.SUPPLIER_ID);
                              UPDATE MD_TBL_PART_REPORT9
                                SET MAPPING_TYPE=W_MAPPING_TYPE -- '1'
                                ,SUPPLIER_CODE=actual_item.SUPPLIER_ID
                                ,LOCAL_P_NO =actual_item.PART_NO --W_ORDER_P_NO -- actual_item.PART_NO
                                 ,ORDER_P_NO =actual_item.PART_NO -- W_ORDER_P_NO -- actual_item.PART_NO
                                WHERE ROWID=W_C1.ROWID ;
                              commit;  
                 END LOOP;
             ELSE 
              W_COUNT  := 0;
             -- pre fix
              SELECT count(1) INTO W_COUNT
                        FROM MD_TBL_PART_MASTER2 mmth_part
                          where SUBSTR(mmth_part.PART_NO,0,length(W_C1.PARENT_PART)) = W_C1.PARENT_PART 
                          and  LENGTH(mmth_part.PART_NO) > 8 ;
                IF W_COUNT > 0 THEN
                    W_MAPPING_TYPE :='3'; 
                    FOR actual_item IN (
                           select *  FROM MD_TBL_PART_MASTER2 mmth_part
                                  where SUBSTR(mmth_part.PART_NO,0,length(W_C1.PARENT_PART)) = W_C1.PARENT_PART 
                                  and  LENGTH(mmth_part.PART_NO) > 8 
                                  and ROWNUM =1
                            )
                    LOOP
                    BEGIN
                        W_ORDER_P_NO  := W_C1.PART_NO;
                        FOR W_ORDER_P_NO_item IN (
                            select mmth_part.PART_NO into  W_ORDER_P_NO FROM MD_TBL_PART_MASTER2 mmth_part
                                where (mmth_part.PART_NO=W_C1.PART_NO --W_C2.PART_NO
                              
                                  )
                            or
                            ( 
                                SUBSTR(mmth_part.PART_NO,0,length(W_C1.PART_NO)) = W_C1.PART_NO
                             
                                and  LENGTH(mmth_part.PART_NO) > 8
                           )
                            and ROWNUM =1
                            )
                        LOOP
                            W_ORDER_P_NO  := W_ORDER_P_NO_item.PART_NO;
                        END LOOP;
                      END;
                             -- dbms_output.put_line('ROWID = '||W_C1.ROWID||' actual_item=' || actual_item.SUPPLIER_ID);
                              UPDATE MD_TBL_PART_REPORT9
                                SET MAPPING_TYPE=W_MAPPING_TYPE -- '1' 
                                ,SUPPLIER_CODE=actual_item.SUPPLIER_ID
                                ,LOCAL_P_NO =actual_item.PART_NO --W_ORDER_P_NO -- actual_item.PART_NO
                                ,ORDER_P_NO =actual_item.PART_NO -- W_ORDER_P_NO -- actual_item.PART_NO
                                WHERE ROWID=W_C1.ROWID ;
                              commit;  
                      END LOOP;
                END IF; 
             END IF;
             -- check mc_appr
             W_MMC_APPR:='';
             SELECT count(1) into W_MMC_APPR_COUNT from MD_TBL_PART_STATUS2 mmc_input
                                    where mmc_input.PART_NO = W_C1.PART_NO;
            IF W_MMC_APPR_COUNT > 0 THEN
                W_MMC_APPR:='*';
             END IF;
             UPDATE MD_TBL_PART_REPORT9
                                SET MMC_APPR=W_MMC_APPR -- '1' 
                                WHERE ROWID=W_C1.ROWID ;
                              commit;  
              
      END;
              
              
             -- dbms_output.put_line('W_COUNT_ALL = '||W_COUNT_ALL);
      FETCH C1 INTO W_C1;
      END LOOP;
      CLOSE C1;
      
      OPEN C_CLEAR_SUPP;
   FETCH C_CLEAR_SUPP INTO W_C_CLEAR_SUPP;
   WHILE NOT(C_CLEAR_SUPP%NOTFOUND)
   LOOP
     W_MAPPING_TYPE :='0';
       BEGIN 
       W_COUNT :=0;
        SELECT count(1) INTO W_COUNT
              FROM MD_TBL_PART_MASTER2 mmth_part
              where mmth_part.PART_NO=W_C_CLEAR_SUPP.PART_NO  
              ; 
         --  dbms_output.put_line('ROWID = '||W_C1.ROWID||' W_COUNT='||W_COUNT);
             
            IF W_COUNT > 0 THEN
            W_MAPPING_TYPE :='1';
                 FOR actual_item IN (
                           select *  FROM MD_TBL_PART_MASTER2 mmth_part
                                  where mmth_part.PART_NO=W_C_CLEAR_SUPP.PART_NO -- W_C1.PART_NO 
                                  and ROWNUM =1
                            )
                 LOOP
                            -- dbms_output.put_line('PART_NO = '||actual_item.PART_NO||' actual_item=' || actual_item.SUPPLIER_ID);
                              UPDATE MD_TBL_PART_REPORT9
                                SET MAPPING_TYPE=W_MAPPING_TYPE,SUPPLIER_CODE=actual_item.SUPPLIER_ID
                                WHERE ROWID=W_C_CLEAR_SUPP.ROWID ;
                              commit;  
                 END LOOP; 
            ELSE  
              W_COUNT  := 0;
             -- pre fix
              SELECT count(1) INTO W_COUNT
                        FROM MD_TBL_PART_MASTER2 mmth_part
                          where SUBSTR(mmth_part.PART_NO,0,length(W_C_CLEAR_SUPP.PART_NO)) = W_C_CLEAR_SUPP.PART_NO 
                          and  LENGTH(mmth_part.PART_NO) > 8 ;
                IF W_COUNT > 0 THEN
                    W_MAPPING_TYPE :='3'; 
                    FOR actual_item IN (
                           select *  FROM MD_TBL_PART_MASTER2 mmth_part
                                  where SUBSTR(mmth_part.PART_NO,0,length(W_C_CLEAR_SUPP.PART_NO)) = W_C_CLEAR_SUPP.PART_NO 
                                  and  LENGTH(mmth_part.PART_NO) > 8 
                                  and ROWNUM =1
                            )
                    LOOP
                             -- dbms_output.put_line('ROWID = '||W_C1.ROWID||' actual_item=' || actual_item.SUPPLIER_ID);
                              UPDATE MD_TBL_PART_REPORT9
                                SET MAPPING_TYPE=W_MAPPING_TYPE -- '1'
                                ,SUPPLIER_CODE=actual_item.SUPPLIER_ID
                               -- ,LOCAL_P_NO =actual_item.PART_NO
                               -- ,ORDER_P_NO =actual_item.PART_NO
                                WHERE ROWID=W_C_CLEAR_SUPP.ROWID ;
                              commit;  
                      END LOOP;
                END IF; 
             END IF;
       
       END;
      FETCH C_CLEAR_SUPP INTO W_C_CLEAR_SUPP;
      END LOOP;
      CLOSE C_CLEAR_SUPP;
      
        
   /* */
     select CURRENT_TIMESTAMP into v_timestamp from dual;
     select PROCESS_STATUS_ID into v_process_status_d from MD_TBL_CONTROL_MAPPING where PROCESS_ID='MAPPING';
      
         update MD_TBL_CONTROL_MAPPING set PROCESS_STATUS_ID=2 where PROCESS_ID='MAPPING';
     
        MD_STP_GEN_DUMMY(P_CRATED_BY,P_CRATED_IP);
        MD_STP_INPUT_STATUS(P_CRATED_BY,P_CRATED_IP);
     commit;
END;